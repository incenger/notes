---
title : Me and My blog
subtitle: Little things about my blog
comments: false
---

Một blog bình thường thì nên có gì?

Tôi không biết. Đó cũng là lí do tại sao bạn có thể tìm được những cơn mưa bất chợt, một li coffee ấm nóng, một bài toán nào đó hay kể cả một vài dòng code.

Tôi đã nhen nhóm ý tưởng tạo cho mình một blog từ lâu. Nhưng sau khi cho ra đời blog đầu tiên của mình, tôi đã để nó mốc meo trong vòng 2 năm. Một phần vì tôi lười, một phần vì lúc đó, tôi vẫn chưa có nhiều cảm nhận, những điều cần phải ghi lại. Blog đầu tiên của tôi ở trên WordPress, nhưng tôi không thực sự thích nó. Sau khi thử qua nhiều platform cho blog, cuối cùng tôi quyết định chọn Hugo. Không biết trong tương lai, tôi có chuyển sang một platform nào khác không, nhưng hiện tại, Hugo cho tôi đủ những gì tôi cần cho blog của mình.

Có những khoảnh khăc trong cuộc sống cần lưu giữ, có những câu chuyện cần sẻ chia. Kí ức con người dường như rất mong manh, những kỉ niệm rồi thì cũng sẽ dần phủ lớp bụi thời gian. Tôi muốn viết ra những điều đó, để sau này đọc lại, tôi biết mình đã hạnh phúc, đã đau khổ như thế nào. Blog này ra đời, là để tôi và cho những ai tình cờ lướt qua những dòng chữ trong này hiểu về tôi hơn.

Tôi viết, chỉ để nhắc mình không lãng quên.

---

Đấy là về cái blog này, còn đây là tôi!
![alt text](https://lh3.googleusercontent.com/Mbj-0-oQtljzMti9rOQK_n0HyVLUN3rhYS2PWxFOodorAYEEACRnYRWn38cxNYuFsq9p4ogJDutj9Nqn3dJhx9ch6vnE5028s1tcIxWQJK2jg8LxokhGD8Qf8y87Bl8H3yS1X2aDatTu4OTXR8yS7pHTfxceSgQpydyYzmMjNOBUh6KD2n7FxEMXgSys7CrXHhbTNIvsZeP0zmqr_leN4sMANPCLOf9vW-nxQ-YA_4r_2CAA76FGE2LYQAkeWdnd-zLu0yiglCK8gDJrNj46kBnRfYUvzPCWCnYH-Yu56iK96DoPt2zrC-kNMe5fFI0TzIGqT1cMa44tIfKMlvEQJXqrWLIVbMtHmqju2W1-Z5tYVnywhiT4xyla5D9w58sMWENNm1y_cAzENi0G-PujE7EoBj6sg2PGVWq44J9ttaElCU4TNa5oS1VzzwIuRfHxNy6yjDL_JsAsYY5FEn90aCfxJKh_rzDS-keGiMDa8cLbt3HIHY7h64Bk_L8ToySIzjgSvWEdpx6-WjEDQgCUPLOpI8RnXIjt8XYlVOYWe7_i7AjS4cL7ufqOESiuRESPHWV-jKLE-cSByUvzJ2FQwNPTKjbkCPy8wdKCRBbyJ829ln3WYlBUOXpos1wcMpxuFO_bRw-U1VWnt8wfE9MtLahK5jKU2eE=w1487-h990-no)
Đề phòng nhầm lẫn thì tôi là cái người đang đứng giữa ảnh ấy :D.
