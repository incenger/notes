---
title: "Introduction"
date: 2019-10-11T07:33:37+07:00
draft: true
tags: ['cs300']
---

<!--more-->

# Software

## Software products

Software are not just computer programs but also include documentation, libraries, support website, and configuration data.

Software engineer are concerned with developing software products (that can be sold to a customer).

* *Generic products*:  Stand-alone systems that are marketed and sold to any customer who is able to buy them. The software developer take in charge of what the software should do and decisions on software changes.
* *Customized products*: Commissioned and developed for a particular customer. The customer decide what the software should do and make decisions on software changes.
  
## Attributes of good software

|Characteristics|Description|
|:-------------|:-----------|
|Maintainability|Software should evolve to meet the changing needs of customers|
|Dependability and Security|Software dependability includes a range of characteristics including reliability, reliability, security and safety. Software has to be secure so that users cannot damage the system|
|Efficiency|Software should not make wasteful use of system resources|
|Acceptability|Software need to be acceptable to the type of users for which it is designed.|

## Software Engineering

Software engineering is an *engineering discipline* that is
concerned with **all aspects of software production** from the early stages of system specification through to maintaining the system after it has gone into use.

## Software costs

Software costs often dominate computer system costs. The costs of software on a PC are often greater than the hardware cost.

Software costs more to maintain than it does to
develop. For systems with a long life, maintenance
costs may be several times development costs.

# Software process

There are different software processes, however they all must include:

* *Software specification:* The functionality of the software and constraints on its operation must be defined.
* *Software development:* The software to meet the specification must be produced.
* *Software validation:* The software must be validated to ensure that it does what the customer wants.
* *Software evolution:* The software must evolve to meet changing customer needs.
  
Some fundamental principles apply to all types of software system, irrespective of the development techniques used:

* Systems should be developed using a managed and understood development process.
* Dependability and performance are important for all types of system
* Understanding and managing the software specification and requirements are important.
* Reuse software rather than write new software.

## Software process model

A software process model is a simplified representation of a software process from a particular perspective.

### The waterfall model

![](../img/2019-11-10-12-43-04.png)


The waterfall model takes the fundamental process activities of specification, development, validation, and evolution and represents them as separate process phases such as requirements specification, software design, implementation, and testing.

The main drawback of the waterfall model is the difficulty of accommodating change after the process in underway. In principle, a phase has to be complete before moving onto the next phase.

Therefore, the waterfall model is appropriate when the requirements are well-understood and changes will be fairly limited during the design process.

### Incremental development

![](../img/2019-11-10-13-11-46.png)

Incremental development is based on the idea of developing an initial implementation, getting feedback from users and others, and evolving the software through several versions.

Incremental development has three major advantages over the waterfall model:

* The cost of implementing requirements changes is reduced. 
* It is easier to get customer feedback on development work that has been done.
* Early delivery and deployment of useful software to the customer is possible. Customers are able to use and gain value from the software earlier.

However, the incremental approach has two problems:

* The process is not visible. Managers need regular deliverables to measure progress. If systems are developed quickly, it is not cost-effective to produce documents that reflect every version of the system
* System structure tend to degrade as new increments are added. Regular change leads to messy code as new functionality is added in whatever way is possible.

### Integration and Configuration

![](../img/2019-11-10-13-20-01.png)

There are there frequently used software components:

* Stand-alone application systems that are configured for use in a particular environment. These systems are general-purpose systems that have many features, but they have to be adapted for use in a specific application.
* Collections of objects that are developed as a component or as a package to be integrated with a component framework.
* Web services that are developed according to service standards and that are available for remote invocation over the Internet.

The advantages of reuse-oriented software engineering:

* Reducing the amount of software to be developed and so reducing cost and risks.
* Faster delivery and deployment of system

Drawbacks:

* Requirements compromises are inevitable, and this may lead to a system that does not meet the real needs of users
* Loss of control over evolution of reused system elements.

## Process activities

### Software specification

Software specification (requirements engineering) is the process of understanding and defining what services are requiring and identifying the constraints on the system's operation and development

Before requirements engineering process starts, feasibility study may be carried out to assess whether or not there is a need or market for the software and whether or not it is technically and financially realistic to develope the software

Requirements are usually presented at two levels of details: High-level statements for customers and detailed specification for developers.


There are three main activities:

![](../img/2019-11-10-14-22-41.png)

* *Requirements elicitation and analysis:* deriving the system requirements through observation of existing systems, discussions with potential users and procurers, task analysis, and so on.
* *Requirements specification:* translating the information gathered during requirements analysis into a document that defines a set of requirements.
* *Requirements validation:* This activity checks the requirements for realism, consistency, and completeness.

### Software design and implementation

The implementation stage of software development is the process of developing an executable system for delivery to the customer

A software design is a description of the structure of the software to be implemented, the data models and structures used by the system, the interfaces between system components and, sometimes, the algorithms used.

![](../img/2019-11-10-14-25-30.png)

* *Architectural design:*  identify the overall structure of the system, the principal components, their relationships, and how they are distributed.
* *Database design:* design the system data structures and how these are to be represented in a database.
* *Interface design:* define the interfaces between system components.
* *Component selection and design:*  search for reusable components and, if no suitable components are available, design new software components.

### Software validation

Verification and validation (V & V) is intended to show that a system both conforms to its specification and meets the expectations of the system customer.

Program testing, where the system is executed using simulated test data, is the principal validation technique.

![](../img/2019-11-10-14-44-48.png)

* *Component testing:* Each component is tested independently, without other system components.
* *System testing:* This process is concerned with finding errors that result from unanticipated interactions between components and component interface problems.
* *Customer testing:* The system is tested by the system customer (or potential customer) rather than with simulated test data.

### Software evolution

![](../img/2019-11-10-14-51-17.png)

## Coping with changes

Change leads to rework so the costs of change include both rework as well as the costs of implementing new functionality.

Approaches to reduce cost of rework:

* *Change anticipation:* software process includes activities that can anticipate or predict possible changes before significant rework is required.
* *Change tolerance:* he process and software are designed so that changes can be easily made to the system.

Ways to coping with changes:
* *System prototyping:*  version of the system or part of the system is developed quickly to check the customer’s requirements and the feasibility of design decisions.
* *Incremental delivery:* system increments are delivered to the customer for comment and experimentation.

### Software Prototyping:

A prototype is an early version of a software system that is used to demonstrate concepts, try out design options, and find out more about the problem and its possible solutions.
