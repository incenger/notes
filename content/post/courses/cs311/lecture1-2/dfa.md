---
title: "General Concepts and Finite States Automata"
date: 2019-10-07T07:45:33+07:00
draft: true
tags: ['cs311']
---

CS311 - Lecture 1&2: Problems, languages, machines, computability, complexity
<!--more-->

- [x] Formal language ? <> Natural language
- [ ] Set, countability of set ($N, Q$): countable and uncountable
- [x] Table-drive and drive code
- [x] Regular operator 

# Some central concepts

## Alphabets

> Alphabet is a finite, non-empty set of symbols. We usually use $\Sigma$ to denote an alphabet.

## Strings

> A string (or word) is a finite sequence of symbols chosen from some alphabets.

### The empty string

> The empty string has zero occurrences of symbols and usually denoted by $\epsilon$

### String length

> The length of a string is the number of positions for symbols in that string.

### Powers of an alphabet

> If $\Sigma$ is an alphabet, we define $\Sigma^{k}$ to be set of strings length $k$ whose symbols chosen form $\Sigma$.

Some common denotations

* Set of all strings excluding the empty string: $\Sigma^{+} = \Sigma^{1} + \Sigma^{2} + \dots$
* Set of all strings over an alphabet $\Sigma$: $\Sigma^{\*} = \epsilon + \Sigma^{+}$

## Languages

> Languages are sets of strings of symbols drawn from some alphabet

**Examples:**

* $L\_1 = \\{  a^{n}b^{n} | n \leq 2 \\}$
* $L\_2 = \\{  a^{n}b^{m} | n, m \geq 2 \\}$

We can use **formal languages** to describe problems. In particular, a decision problem such as "Is $P$ true?" can be **reduced** to language membership problems: "Is the **encoding** of $P$ exists in language $L$?".

For instances, we can encode a given configuration $C$ in Tic-tac-toe game as a string and define the language $L = $ set of configuration strings for which the next player can always win. As a result, our question becomes "Is the encoding of $C$ in $L$". 


# Deterministic Finite State Automata

## Formal Definition

> $(Q, \Sigma, \delta, q_0, F)$
> 
> A finite set of states: $Q$
> 
> A finite set of input symbols: $\Sigma$
> 
> A transition function $\delta$ that takes a state and a input symbol and return a state
> 
> A initial state: $q\_0$
> 
> A set of final states: $F (F \in Q)$

## How DFA processes a string

The "language" of the DFA is the set of strings that it accepts. DFA accepts a string if it ends up at one of the final state in $F$ when reading the string.

If a language $L$ is the language for a DFA $A$, then we say $L$ is a **regular language**.

(Formal definition: Textbook or Slide)

## Simpler notations for DFA

We usually use *transition diagram* and *transition table* to describe a DFA.

(Formal definition: Textbook)

**Transition Diagram:**

*We need to specify all possible transition at each state include dead states if necessary.*

![](../img/2019-10-11-16-38-49.png)

**Transition Table:**

![](../img/2019-10-11-16-39-28.png)

## Extending $\delta$ to strings
(Textbook)


## Regular operators

> Suppose $L\_1 = L(M_1)$ and $L\_2 = L(M_2)$, then there exists $M$ such that $L(M) = L_1 \cup L_2$. We say that the set of regular language is closed under union.

The idea is running the string through both $M_1$ and $M_2$ simultaneously. Each state of $M$ is a pair of stats of $M_1$ and $M_2$.

The same idea applies for two operations:

* Complementation ( combined with union $\rightarrow$ intersection)
* Concatenation
* Star (repeat $\geq 0$)

> The set of regular languages is closed under regular operators



## Coding up DFAs

Directed-code and table-driven approach (Slides)