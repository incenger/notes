---
title: "Nondeterministic Finite State Automata"
date: 2019-10-14T07:58:30+07:00
draft: true
tags: ['cs311']
---
Lecture 3

- [ ] Table-driven and driver code
- [ ] From NFA $\rightarrow$ DFA
- [ ] 
- 

<!--more-->

# Nondeterminism

In NFA, we have a **set** of choice (zero, one or more possible transitions) at each step.

**Formal definition**: Textbook

Differences:

* Multiple transition on given input
* Some states have **no** transition on an input
* Some transition labeled $\epsilon$ (accepts nothing to move to the next state)

## NFA Computation

NFA clones itself and each clone explore one possibility.Some of those clones can reach a "dead end" and stuck. However, NFA accepts input string if any of those clones reaches a final state.

### Concatenation with NFAs

![](../img/2019-10-14-08-49-48.png)

### Building *-closure with NFAs

![](../img/2019-10-14-08-51-11.png)

### Union with NFAs

![](../img/2019-10-14-08-52-04.png)

# NFA and DFA equivalence 

> Every languages that can be described by a NFA can also be described by an equivalent DFA.

## Subset Construction (without $\epsilon$ transition)

Let $N = (Q\_N, \Sigma, \delta\_N, q\_0, F\_N)$ is a NFA with no $\epsilon$ transition that recognize $L$, we aim to build a DFA $D = (Q\_D, \Sigma, \delta\_D, {q\_0}, F\_D)$ that $L(N) = L(D)$

* $Q\_D$ is the set of subsets (power set) of $Q\_N$. In the worst case, if $Q\_N$ has $n$ states, $Q\_D$ can have $2^n$ states. However, usually, not all of those cases are accessible and we often omit inaccessible states.
* $F\_D$ is all set of $N$'s states that include at least one accepting state of $N$.
* For each set $S \subseteq Q\_N$ and for each input symbol $a$, we look at all the states in $S$, see which states in $N$ input $a$ can bring us to and union all such states.
![](../img/2019-10-14-15-26-32.png) 

**Proof**: Induction of length of input string (Textbook).


## $\epsilon$-closure

$\epsilon(s)$ is the set of states reachable from $s$ by traveling $0$ or more $\epsilon$ edges

## Subset Construction ($\epsilon$-NFA)

Expanding the power set $Q$, initial state $q_0$ and transition function $\delta$ using $\epsilon$-closure
