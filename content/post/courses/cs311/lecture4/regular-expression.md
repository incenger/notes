---
title: "Regular Expression"
date: 2019-10-14T10:39:22+07:00
draft: true
tags: ['cs311']
---

Lecture 4

<!--more-->

# Finite Automata and Regular Expression

Regular expression represent the same set of languages of Finite Automata: regular languages:

> Every language defined by one of these automata is also de fined by a regular expression. For this proof we can assume the language is accepted by some DFA.

> Every language defined by a regular expression is defined by one of these automata. For this part of the proof the easiest is to show that there is an NFA with $\epsilon$ transitions accepting the same language.

![](../img/2019-10-28-07-50-43.png)

## From DFA to RE

### Using induction

**Proof** (Textbook)

**Idea:**

Let $R\_{ij}$ be the R.E whose language is the set of string $w$ such that $w$ is the label of the path from state $i$ to state $j$. Suppose $i=1$ is start state and $\\{ f_1, f_2, \dots, f_m \\}$ is the set of accepting state, then then regular language 

$$L = L(A) = L( R ) = R\_{1 f\_1} + R\_{1 f\_2} + \dots + R\_{1 f\_m}$$

Let $R\_{ij}^{(k)}$ be the R.E whose language is the set of string $w$ such that $w$ is the label of the path from state $i$ to state $j$ that use intermediate nodes whose number is not greater than $k$ ($i$ and $j$ could be larger than $k$), then $R\_{ij} = R\_{ij}^{(n)}$. We use induction to construct $R\_{ij}^{(k)}$ from $k = 0$ to $k=n$.

**Induction step:**

From $R\_{ij}^{(k-1)}$, we have two possible way to construct $R\_{ij}^{(k)}$

* Do not use the state $k$ at all. 
* Using the state $k$ at least once.

![](../img/2019-10-28-08-59-01.png)

Combine two above cases, we have:

![](../img/2019-10-28-08-59-37.png)

**Example:** Textbook

### By Eliminating states 

When eliminate a state $s$, we must include on an arch that goes directly from $q$ to $p$ the labels of paths that go from $q$ to $p$ through $s$. Since we cannot not use a single character tor represent the label of the arch, we need to use a R.E. Thus, we are led to consider a automata that have R.E as labels.

![](../img/2019-10-28-10-02-12.png)

* $q_1, q_2, \dots, q_k$: predecessor of $s$.
* $p_1, p_2, \dots, q_m$: successors of $s$.
* $Q\_i, P\_1, R\_{ij}$: R.E labels for the arc.

We need to compensate for all arcs involving $s$.

* For each path from $q_i$ to $p_j$ that go to $s$ zero or more times, the expression for this path is $Q\_iS^{*}P\_j$. This expression is added (with union operator) to the arc from $q_i$ to $p_j$ (if there was no arc from $q_i \rightarrow p_j$, we introduce one with $\emptyset$ label)

![](../img/2019-10-28-10-15-32.png)

## Converting R.E to Automata

We start by showing how to construct an automata for the basic expression: single symbol, $\epsilon$, and $\emptyset$. We then show how to combine these automata into larger automata that accepts closure of the language accepted by smaller automata.

**Proof:** Textbook.

# Algebraic laws for R.E

**TEXTBOOK**