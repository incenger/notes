---
title: "Properties of Regular Language"
date: 2019-11-02T10:44:31+07:00
draft: true
tags: ['cs311']
---

<!--more-->

# Proving Languages Not to Be Regular

## The Pumping Lemma 

> Let L be a regular language. Then there exists a constant $n$ which depends on $L$ such that for
every string $w$ in $L$ such that $|w| \geq n$, we can break $w$ into three strings $w = xyz$, such that:

> 1. $y \neq \epsilon$
> 2. $|xy| \leq n$
> 3. For all $k \geq 0$, the string $xy^kz$ is also in $L$

**PROOF:** Textbook

![](../img/2019-11-02-10-53-56.png)

## Applications of Pumping Lemma

### Pumping Lemma as an Adversarial Game

> 1. Player  picks the language $L$ to be proved nonregular.
> 2. Player 2 picks $n$ but doesn't reveal to player $1$ what $n$ is;  player $1$ must devise a play for all possible $n$'s.
> 3.  Player $1$ picks $w$ which may depend on $n$ and which must be of length at least $n$.
> 4. Player $2$ divides $w$ into $x$ $y$ and $z$ obeying the constraints that are stipulated in the pumping lemma
$y \neq \epsilon$ $|xy| and \neq n$. Again player $2$ does not have to tell player $1$ what $x$, $y$ and $z$ are although they must obey the constraints.
> 5. Player $1$ wins by picking $k$ which may be a function of $n$, $x$, $y$ and $z$ such that $xy^kz$ is not in $L$.

### Proving a Language is not a R.L

**Example:** Let us show that the language $L_{pr}$ consisting of all strings of $1$'s whose length is a prime is not a regular language. Suppose it were. Then
there would be a constant $n$ satisfying the conditions of the pumping lemma. Consider some prime $p \geq n+2$, there must be such a $p$, since there are an infinity prime. Let $w = 1^p$.

By the pumping lemma, we can break $w = xyz$ such that $y \neq epsilon$ and $|xy| \neq n$. Ley $|y| = m$. Then $|xz| = p - m$. Now considering the string $xy^{p-m}z$, which must be in $L_{pr}$ by the pumping lemma. However, 
$$|xy^{p-m}z| = |xz| + (p-m)|y| = p-m + (p-m)m = (p-m)(m+1)$$

$m + 1 > 1$, since $y \neq \epsilon$ tell us $m \geq 1$.
Also, $p - m >1$, since $p \geq n+2$ was chosen, and $m \neq n$ since $m = |y| \neq |xy| \neq n$. Therefore, $|xy^{p-m}z|$ has two different factions larger than $1$, and that leads to a contradiction. Thus, we conclude that $L_{pr}$ is not a regular language.

