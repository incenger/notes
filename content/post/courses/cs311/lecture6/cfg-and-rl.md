---
title: "Context-Free Grammar and Regular Language"
date: 2019-11-04T07:40:42+07:00
draft: true
tags: ['cs311']
---

<!--more-->

# Context-Free Grammar

## Definition

We present CFG by four components, that is, $G = (V, T, P, S)$:

* $V$: set of variables - each variable represents a set of string.
* $T$: the terminals -  the finite set of symbols that form the string of language being defined.
* $P$: the set of productions. Each production consists of:
  * A variable that is being (partially) defined by the production.
  * The production symbol $\rightarrow$.
  * Body of production, which is a string of zero or more terminals and variables.
* $S$: start symbol - The variable represents the language being defined. Other variables represent auxiliary classes of string  that are used to define the language of start symbol.

## Derivation using a grammar

We derive a string of terminals by repeatedly applying rules beginning from a designated start variable.

## Leftmost and Rightmost Derivation

In order to restrict the number of choices we have in deriving a string, it is often useful to require that at each step we replace the leftmost (or rightmost) variable by one of its production rules.

## From NFA to Regular Grammar

> 1. Rename states $Q$ to a set of upper-case letters (Convert states to variables).
> 2. That start symbol of the grammar is the name of the start state $q_0$
> 3. For each transition $I \xrightarrow{a} J$, create a production $I \rightarrow aJ$.
> 4. For each transition $I \xrightarrow{\epsilon} J$, create a production $I \rightarrow J$.
> 5. For each final state $K$, create the production $K \rightarrow \epsilon$

## From Regular Grammar to NFA

> 1. Transform the RG so that all productions are of the form $A \rightarrow x$ or $A \rightarrow xB$ where $x$ is either single letter or $\epsilon$
> 2. The start state of the NFA is the grammar's start symbol
> 3. Create state $Q_F$ and add it to the set $F$ of final states.
> 4. For each production $I \rightarrow aJ$, create a transition $I \xrightarrow{a} J$
> 5. For each production $I \rightarrow J$, create a transition $I \xrightarrow{\epsilon} J$
> 6. For each production $K \rightarrow \epsilon$, add $K$ to the set of final states
> 7. For each production $I \rightarrow a$, create a transition $I \xrightarrow{a} Q_F$

# Language Indistinguishability

> Consider a language $L$ over an alphabet $A$.

> Two strings $x, y \in A^\*$ are $L-$ indistinguishable if for all $z \in A^*$, $xz \in L$ whenever $yz \in L$. We denote $x \equiv_{L} y$.

> The index of $L$ is the number of equivalence classes induced by $\equiv\_L$.

## Myhill-Nerode Theorem

## Minimum state DFA

For any language $L$, there is a *unique* minimum-state DFA that recognizes $L$. Any DFA can be transformed into minimum state DFA.

### Equivalent states

> Two states $p$ and $q$ is a DFA are equivalent if for all $z \in \Sigma^\*$, $\hat{\delta}(p, z)$ is a final state exactly when $\hat{\delta}(q, z)$ is a final state.

### Table-filling algorithms

To find states that are equivalent, we make our best efforts to find pairs of states that are distinguishable.

> **BASIS:** If $p$ is accepting state and $q$ is nonaccepting, then the pair $\{ p, q\}$ is distinguishable.

> **INDUCTION:** Let $p$ and $q$ be states that for some input symbol $a$, $r = \delta(p, a)$ and $s = \delta(q, a)$
where $r$ and $s$ are pair of states known to be distinguishable. Then $\{p, q\}$ is a pair of distinguishable.

![](../img/2019-11-11-10-39-17.png)

* $x$ indicates pairs of distinguishable states, and the blank squares indicate those pair that have been found equivalence.
* Initially, there's no $x$ in the table.