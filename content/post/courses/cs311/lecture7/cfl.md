---
title: "Context-Free Language: Grammars and Automata"
date: 2019-11-18T07:33:22+07:00
draft: true
tags: ['cs311']
---

<!--more-->

# Context-free Grammar

## Formal definition

## Derivation

### Equivalence between derivation and parse tree

### Leftmost Derivations

### Ambiguity via left-most derivations

# Context-free Languages

## Pumping Lemma

> If $L$ is a context-free language, then there is a number $p$ (pumping length) such that for all strings $z \in L$ and $|z| \geq p$, $z$ can be divided into 5 pieces $z = uvwxy$ satisfying:
>
> * For each $i \geq 0$, $uv^iwx^iy \in L$ 
> * $|vx| > 0$
> * $|vwx| \leq p$

# Pushdown Automata

The pushdown automata recognizes the context-free language. It is an extension of $\epsilon-$NFA with an additional stack 

## Definition

### Informal Introduction

The presence of a stack means that, unlike the finite automata, the pushdown automaton can remember an infinite amount of information.

![](../img/2019-11-18-07-55-55.png)

In one transition, the pushdown automaton:

* Consumes the input symbol for transition.
* Goes to new state
* Replaces the symbol at the top of stack by any string. 

### Formal Definition

> $P = (Q, \Sigma, \Gamma, \delta, q\_0, Z\_0, F)$
> 
> $Q$: A finite set of states.
> 
> $\Sigma$: A finite set of input symbol.
> 
> $\Gamma$: A finite stack alphabet.
> 
> $\delta$: The transition function. $\delta$ takes an argument of a triple $(q, a, X)$, where:
> 
> * $q \in Q$
> 
> * $a = \epsilon$ or $a \in \Sigma$
> 
> * $X \in \Gamma$
> 
> The output of $\delta$ is a finite set of pair $(p, \gamma)$ where $p$ is the new state and $\gamma$ is the string of stack symbols that replaces $X$ at the top of the stack.
> 
> $q\_0$: The start state
> 
> $Z\_0:$ The start symbol. Initially, the stack consists of one instance of this symbol.
> 
> $F$: Accepting states.

### Graphical Notation

The transition diagram for PDA:

* The nodes correspond to states of the PDA
* An arrow labeled indicating the start state and doubly circled states are accepting states.
* The arcs correspond to transitions of the PDA in following sense. An arc labeled $a, X/\alpha$ from the state $q$ to the state $p$ means that $\delta(q, a, X)$ contains the pair $(p, \alpha)$

![](../img/2019-11-18-08-34-59.png)

## The languages of a PDA

There are two acceptances for PDA:

* Acceptance by final states.
* Acceptance by empty stack.

These two methods are equivalent and be converted from one to another.

### From Empty Stack to Final State

![](../img/2019-11-18-09-58-26.png)

* We use a new symbol $X\_0$ (not a symbol of $\Gamma$). $X_0$ is both start symbol of $P_F$ and a marker on the bottom of the stack that lets us known when $P\_N$ reaches empty stack.
* We need a new start state $p\_0$ whose sole function is to push $Z\_0$, the start symbol of $P\_N$ onto the top of the stack and enter state $q\_0$, the start state of $P\_N$. 
* Then $P\_F$ simulates $P\_N$ until the stack of $P\_N$ is empty.
* Finally, we add another new state $p\_f$, which is the accepting state of $P\_f$.

$$P\_F = (Q \cup \\{p\_0, p\_f\\}, \Sigma, \Gamma \cup \\{X\_0 \\}, \delta\_F, p\_0, \\{p\_f\\})$$

$\sigma\_F$ is defined:

* $\sigma\_F(p\_0, \epsilon, X\_0) = \\{(q\_0, Z\_0X\_0)\\}$
* For all states $q \in Q$, inputs $a$ in $\Sigma$ or $a = \epsilon$ and stack symbol $Y \in \Gamma$, $\delta\_F(q, a, Y)$ contains all the pairs in $\delta\_N(q, a, Y)$
* $\delta\_F(q, \epsilon, X\_0)$ contains $(p\_f, \epsilon)$ for every state $q \in Q$.

### From Final State to Empty Stack

![](../img/2019-11-18-10-32-18.png)

From each accepting state of $P\_F$, add a transition on $\epsilon$ to a new state $p$. When in state $p$, $P\_N$ pops its stack and does not consume any input. To avoid simulating a situation where $P\_F$ accidentally empties its stack without accepting, $P\_N$ must also use a marker $X\_0$ on the bottom of the stack.

$$P\_N = (Q \cup \\{p\_0, p\\}, \Sigma, \Gamma \cup \\{ X\_0\\}, \delta\_N, p\_0, X\_0)$$

where $\delta\_N$ is defined:

* $\delta\_N(p\_0, \epsilon, X\_0) =\\{ (q\_0, Z\_0X\_0) \\}$.
* For all states $q \in Q$, input symbol $a \ in \Sigma$ or $a = \epsilon$ and $Y \in \Gamma$, $\delta\_N(q, a, Y)$ contains all the pairs in $\delta\_F(q, a, Y)$
* For all accepting states $q \in F$ and stack symbols $Y \in \Gamma$ or $Y = X\_0$, $\delta\_N(q, \epsilon, Y)$ contains the pair $(p\_0, \epsilon)$
* For all stack symbols $Y \in \Gamma$ or $Y = X\_0$, $\delta\_N(p, \epsilon, Y) = \\{ (p, \epsilon)\\}$



# CFG and PDA Equivalence


## Convert from PDA to CFG

> Key idea: Each string derived from $A_{pq}$ is capable of taking the PDA from the state $q$ to state $p$ with empty stack.

Start by modifying PDA so that:

* It has single state $\sigma$ and single final state $\phi$.
* Start and finish with empty stack
* Each transition must push or pop a single symbol onto stack.
  
To construct the CFG:

* For each state $p \in Q$, the rule $A_{pp} \rightarrow \epsilon$ is added.
* For each $p,q,r \in Q$, the rule $A_{pq} \rightarrow A\_{pr}A\_{rq}$ is added.
  
![](../img/2019-12-30-16-26-01.png)

* For each $p, q, r, s \in Q$, the rule $A_{pq} \rightarrow aA\_{rs}b$ is added if in $p$ and $q$, the PDA push and pop the same stack symbol.($a, b$ are the input symbol at $p$ and $q$) 

![](../img/2019-12-30-16-30-13.png)



