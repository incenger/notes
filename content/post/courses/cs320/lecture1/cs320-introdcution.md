---
title: "CS 320 Lecture 1: Introduction"
date: 2019-10-09T07:48:07+07:00
draft: true
tags: ['cs320']
---

<!--more-->

- [ ] Textbook: concepts of programming language
- [ ] Why we have many PLs? 3 reasons
- [ ] 5 paradigms

# Multiple programming diagrams

## Imperative Paradigm

> Imperative paradigm focuses on **how** to execute, defines control flow as statements that change a program **state**.

* Underlying notion of an abstract machine
  * Von Neumann architecture
  * Key operation: assignment

## Object-oriented paradigm

> OOP based on the concept of objects - which have attributes and method. In OOP, computers are designed by making them out of objects that interact with others. 

* Data abstraction and information hiding
* Actions performed on objects
* Key operation: Message passing - The invoking program sends a message and relies on the object to select and execute the appropriate code.

## Functional Paradigm

> Treats computation as the evaluation of mathematical functions avoiding state and mutable data

* (Pure) value binding through parameter passing
* Lambda calculus
* No iteration
* No store accessible through names
* Key operation: function application (with recursion)

## Logic Paradigm

> Program is a formal description of characteristics required of a problem solution.

* Programs tell *what should be* not *how to make it* 
* Solutions through reasoning process.
* Key operation: unification

# What makes a Good Programming Language

* Ease of **programming**: 
  * Increase productivity
  * Good syntax design, high expressive power
* Ease of **implementation**: 
  * Simpler compiler/interpreters needed, faster software development cycles
  * Good syntax design (simpler grammar), no expensive construct (control structure)
* Ease of **Code Reuse** and **Maintenance**
  * Help increase productivity and reliability of programs
  * Good abstraction, language standardization
* Efficient **Target Code**: 
  * Performance (usually in terms of speed and memory) is the bottom line for many applications
  * More static features, less dynamic features
  * More stack storage, less heap storage
* High **security**
  * Strong type system, automatic memory management, better exception handling ability