---
title: "Lexical and Syntax Analysis"
date: 2019-10-30T07:31:49+07:00
draft: true
tags: ['cs320']
---

<!--more-->

# Lexical Analysis

A lexical analyzer is essentially a pattern matcher. It serves as the front-end of a syntax analyzer. 

An input program appears to a compiler as a single string of characters. The lexical analyzer collects characters into logical groupings (**lexemes**) and assigns internal codes (**tokens**) to grouping according to their structure.

Consider the assignment statement

```cpp
result = oldsum - value/100;
```

The tokens and lexemes of this statement

| Token     | Lexeme |
| --------- | ------ |
| IDENT     | result |
| ASSIGN_OP | =      |
| IDENT     | oldsum |
| SUB_OP    | -      |
| IDENT     | value  |
| DIV_OP    | /      |
| INT_LIT   | 100    |
| SEMICOLON | ;      |

The lexical analysis process includes skipping comments and whitespace outside lexemes, as they are not relevant to the meaning of the program. Also, the lexical analyzer inserts lexemes for user-defined names into symbol table, which is used by later phases of compiler. Finally, it detects syntactic errors in tokens and report such errors to the users.

A token is typically represented as an object with attributes:

* **Type** - the key info used by a grammar
* **Lexeme** - to distinguish individual member of a group
* **Value** - for numerical tokens
* **Position** - line and column numbers

