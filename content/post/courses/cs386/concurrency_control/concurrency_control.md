---
title: "Concurrency Control"
date: 2019-07-27T08:58:04+07:00
lastmod: 2019-07-27T08:58:04+07:00
description: ""
tags: ['CS386']
---

Notes on Concurrency Control

<!--more-->

 ![](2019-07-27-09-02-24.png)

# Serial and Serializable Schedules
 
We need to consider "schedules" of actions that can be guaranteed to produce the same result as if the transactions executed _one-at-a-time_.

## Schedules

A __schedule__ is a time-ordered sequence of the _important actions_ taken by one or more transactions. When studying concurrency control, only the __READ__ and __WRITE__ actions, and their orders, are important.

In examples below, we assume that the only consistency constraint on the database is that $A=B$.

### Serial Schedules

A schedule is __serial__ if its actions consist of all the actions of transaction, then all the actions of other transaction, with no mixing of the actions.

![](2019-07-27-09-13-30.png "Serial schedule example")

Every serial schedule will _preserver consistency_ of the database state.

### Serializable Schedules

A schedule is __serializable__ if its effect on the database state is the same as that of some serial schedule, regardless of what the initial state of the database is.

![](2019-07-27-09-19-16.png "A seiralizable, but not serial, schedule")

### The effect of transaction semantics

 We need to consider in detail the operations performed by the transactions, to determine whether or not a schedule is serializable.

 For example, consider two schedule below. They only differs in the multiplier of $s$, however, one is serializable and other is not.

 Nonserializable             |  Serializable
:-------------------------:|:-------------------------:
![](2019-07-27-09-26-29.png "A nonserializable schedule")  |  ![](2019-07-27-09-27-01.png "A schedule that is serializable ony because of detailed behavior of transactions")


It is not realistic for the scheduler to concern with the details of computation undertaken by transactions. However, the scheduler needs to get to see the read and write requets from the transactions. Therefore, we need to make an assumption:

> If there is something that transaction $T$ could have done to $A$ that will make the database state inconsistent, then $T$ will do that.

### Notations for Transactions and Schedules

Transactions and schedules are represented by shorthand notation, in which actions are $r\_T(X)$ and $w\_T(X)$, meaning that transaction $T$ reads, or writes, database element $X$.

A __transaction__ is a sequence of actions.

A __schedule__ $S$ of a set of transactions $\tau$ is a sequence of actions, in which for each transaction $T\_i$ in $\tau$, the actions of $T_i$ appear in $S$ __in the same order__ that they appear in the definition of $T_i$ itself. $S$ is an __interleaving__ of actions of the transactions.


# Conflict-Serializability

The _conflict-Serializablity_ is based on idea of a __conflict__: a pair of consecutive actions in a schedule such that, if their order is interchanged, then the behavior of a least of the transactions involved can change.


## Conflicts

Consider two different transaction $T_i$ and $T_j$

__Not conflict:__

*  $r_i(X)$, $r_j(Y)$ is never a conflict __even if $X=Y$__ because neither of these actions change the value of any database elements.

* $r_i(X)$, $w_j(Y)$, $w_i(X)$, $r_j(Y)$, or $w_i(X)$, $w_j(Y)$ are not conflicts provided $X \neq Y$. The reason is that two actions are performed on different database elements.

__Conflict:__

* Two actions of the same transactions cannot be swap. E.g: $r_i(X)$, $w_i(Y)$

* Two writes of the same database element by different transactions. E.g: $w_i(X)$, $w_j(X)$

* A read and a write of the same database element by different transactions. E.g: $r_i(X)$, $w_j(X)$

Two actions of different transactions may be swapped unless:

> They involve the same database element, and at least on is a write.

We can make as many nonconflicting swaps as we wish to turning the schedule into a serial schedule.

Two schedules are __conflict-equivalent__ if they can be turned one into other by sequence of nonconflicting swaps of adjacent actions.

A schedule is __conflict-serializable__ if it is conflict-equivalent to a serial schedule.
 
Conflict-serializable $\Rightarrow$ serializable. Howevere, serializable does not $\Rightarrow$ conflict-serializable

 Serial          |  Serializable but not conflict-serializable
:-------------------------:|:-------------------------:
  ![](2019-07-27-10-46-10.png) |   ![](2019-07-27-10-45-15.png)  

## Precedence Graph and Test for Conflict-Serializability

The idea for conflict-serializability test is that when there are conflicting actions that appear anywhere in $S$, the transactions performing those actions must appear in the same order in any conflict.

Given a schedule $S$ involving transactions $T_1$ and $T_2$, we say $T_1$ takes precedence over $T_2$, written $T\_1 <\_{S} T\_2$, if there are actions $A_1$ of $T_1$ and $A_2$ of $T_2$, such that:

* $A_1$ is ahead of $A_2$ in $S$
* Both $A_1$ and $A_2$ involve the same database element
* At least one of $A_1$ and $A_2$ is a write action

### Precedence Graph

The nodes are transaction. There is an arc from node $i$ (stands for transaction $T_i$) to node $j$ if $T\_i <\_{S} T\_j$

__Example:__
The schedule $S$ has
$r_2(A), r_1(B), w_2(A), r_3(A), w_1(B), w_3(A), r_2(B), w_2(B)$

| $T_1$    | $T_2$    | $T_3$   |
| ---------|----------|---------|
|          | $r_2(A)$ |         |
| $r_1(B)$ |          |         |
|          | $w_2(A)$ |         |
|          |          |$r_3(A)$ |
|$w_1(B)$  |          |         |
|          |          |$w_3(A)$ |
|          | $r_2(B)$ |         |
|          | $w_2(B)$ |         |

The action $r_1(B)$ comes before $w_2(B)$, thus, $T\_1 <\_{S} T\_2$. $w_2(A)$ comes before $r_3(A)$, hence, $T\_2 <\_{S} T\_3$. These are only arcs we can justify from the order of actions in $S$.

 ![](2019-07-27-12-56-35.png)

### Conflict-Serializability Test

 > Construct the precedence graph for $S$ and check for any cycles.

### Proof Precedence Graph Test

Read textbook.

