---
title: "Integrity Constraint"
date: 2019-08-06T09:17:43+07:00
draft: false
tag: ['cs386']
---

<!--more-->

# Characteristics

## Context

> The context of an integrity constraint are relations in which integrity constraint are likely to be violated when we perform modifications.

__Example:__

The constraint: _"The salary of an employee cannot be larger than the salary of the manager"_. We are likely to violate this constraint when:

* Modify the salary of an employee
* Insert employees into relations
* Appoint an employee to be a manager.

$\Rightarrow$ Context is: __EMPLOYEE__, __DEPARTMENT__ relations.

## Content

Content of an integrity constraint can be stated by

* __Natural Language__ which is easy to understand but lack of coherence.

* __Formal Language__ which is condensed, coherent but sometime is difficult to understand. It is represented via

    * Relation Algebra
    * Relation Calculus
    * Pseudocode

__Example__

1. Consider the constraint _"The salary of an employee cannot be larger than the salary of the manager"_, its content written in __formal language__ is:

$$\forall t \in  EMPLOYEE  (\exists u \in  DEPARTMENT (\exists v \in EMPLOYEE ( u.MGRSSN = v.SSN \wedge t.DNO = u.DNUMBER \wedge t.SALARY \leq v.SALARY)))$$

2. _"The supervisor of an employee must be an employee in the company"_ in __formal language__ is:

$$\forall t \in EMPLOYEE (t.SUPERSSN = NULL \vee \exists s \in EMPLOYEE (t.SUPERSSN = s.SSN))$$

## Table of Purview

__Table of Purview__ is used to determine which modifications need to be checked for the integrity constraints when they are performed on some relations.

### One Integrity Constraint

| Constraint Name | Insert | Delete | Update        |
|-----------------|--------|--------|---------------|
| Relation 1      |    +   |    -   | + (Attribute) |
| ...             |   ...  |   ...  |      ...      |
| Relation $n$    |    -   |    +   |       -       |

* (+): has potential to violate the constraints
* (-): do not violate the constraints

## Many Integrity Constraints

__Synthesis Table:__ Combine many table of one integrity constraint

![](2019-08-06-09-46-19.png)

# Categories

## One relation

### Domain 

> Constraint on values of some attributes in a relations.

__Example:__ _"The working hour for one project is not over 60 hours"_

* Context: WORKS_ON
* Content: $\forall t \in \text{WORKS_ON} (t.HOURS \leq 60)$
* Table of purview:

|          	| Insert 	| Delete 	| Update    	|
|----------	|--------	|--------	|-----------	|
| WORKS_ON 	|    +   	|    -   	| + (HOURS) 	|

### Correlated Tuples

> The existence of one or more tuples depends on the existence of other tuples in the same relations

Special cases are __Primary Key Constraint__ and __Unique Constraint__

__Example:__ _"The department name is unique"_

* Context: DEPARTMENT
* Content: $\forall t_1, t_2 \in DEPARTMENT(t_1 \neq t_2 \Rightarrow t_1.DNAME \neq t_2.DNAME)$
* Table of purview:

|            	| Insert 	| Delete 	| Update    	|
|------------	|--------	|--------	|-----------	|
| DEPARTMENT 	|    +   	|    -   	| + (DNAME) 	|


### Correlated Attributes

> Constraints among attributes in a relation

__Example:__ _"An employee does not supervise himself/herself"_

* Context: EMPLOYEE

* Content: $\forall t \in EMPLOYEE(t.SUPERSSN \neq t.SSN)$

* Table of purview:

|          	| Insert 	| Delete 	| Update       	|
|----------	|--------	|--------	|--------------	|
| EMPLOYEE 	|    -   	|    -   	| + (SUPERSSN) 	|


## Referential Constraint

> The value of attributes in some relation must refer to the value of the primary key of other relations. Sometimes, referential constraint can happen on the same relation.

Special case is __Foreign key constraint__

__Example:__ _"Every dependent must has a relationship with an employee"_

* Context: DEPENDANT, EMPLOYEE

* Content: $\forall t \in DEPENDANT(\exists s \in EMPLOYEE(s.SSN = t.ESSN))$

* Table of purview:

|           	| Insert 	| Delete 	| Update   	|
|-----------	|--------	|--------	|----------	|
| EMPLOYEE  	|    -   	|    +   	|  + (SSN) 	|
| DEPENDANT 	|    +   	|    -   	| + (ESSN) 	|

## Many Relations

### Correlated Tuples

> Constraints happen among tuples on many different relations

__Example:__

Consider the relations:
* INVOICE (<ins>Number</ins>, CustID, Date)
* DETAIL(<ins>InvNumber</ins>, <ins>PNumber</ins>, Unit_Price, Quantity)

Constraint: _"Every invoice must have at least one detail"_

* Context: INVOICE, DETAIL

* Content: $\forall t \in INVOICE (\exists s \in DETAIL(t.NUMBER = s.InvNumber))$

* Table of purview:

|         	| Insert 	| Delete 	| Update        	|
|---------	|--------	|--------	|---------------	|
| INVOICE 	|    +   	|    -   	|   + (NUMBER)  	|
| DETAIL  	|    -   	|    +   	| + (InvNumber) 	|

### Correlated Attributes

> Constraints among attributes of many different relations

__Example:__ _"The birth date of a manager is smaller than his/her starting date"_

* Context: EMPLOYEE, DEPARTMENT

* Content: $$\forall t \in DEPARTMENT(\exists s \in EMPLOYEE(s.SSN = t.MGRSSN \wedge t.STARTDATE > s.BIRTHDAY))$$

* Table of purview:

|            	| Insert 	| Delete 	| Update                	|
|------------	|--------	|--------	|-----------------------	|
| EMPLOYEE   	|    -   	|    -   	|   + (BIRTHDAY, SSN)   	|
| DEPARTMENT 	|    +   	|    -   	| + (STARTDATE, MGRSSN) 	|

### Aggregate Attributes

__Aggregate attribute__ has its value calculated from values of other attributes

> Constraints between the aggregate attribute and source attributes.

__Example:__
Suppose each department has attribute No_Emp which is the total number of employees of that department. 

Constraint: _"The value of No\_Emp of a department is equal to the total number of employees of that department from the relation employee"_

* Context: EMPLOYEE, DEPARTMENT

* Content:
$$\forall t \in DEPARTMENT (t.\text{NO_EMP} = card(\\{s \in EMPLOYEE | s.DNO = t.DNUMBER\\}))$$

* Table of purview:

|            	| Insert 	| Delete 	| Update               	|
|------------	|--------	|--------	|----------------------	|
| EMPLOYEE   	|    +   	|    +   	|        + (DNO)       	|
| DEPARTMENT 	|    -   	|    -   	| + (NUM\_EMP, DNUMBER) 	|

### Cycle

 ![](2019-08-06-10-37-52.png)

__Example:__

_"Employees are only assigned to projects that are controlled by their department"_

* Context: EMPLOYEE, PROJECT, WORKS\_ON

* Content:

EMP_PRO $\leftarrow$ EMPLOYEE $\bowtie\_{DNO = DNUM}$ PROJECT
$$ \forall t \in \text{WORKS\_ON} (\exists EMP_PRO (t.ESSN = s.SSN \wedge t.PNUM = s.PNUMBER)) $$

* Table of purview:

|           	| Insert 	| Delete 	| Update            	|
|-----------	|--------	|--------	|-------------------	|
| EMPLOYEE  	|    -   	|    -   	|    + (SSN, DNO)   	|
| PROJECT   	|    -   	|    -   	| + (PNUMBER, DNUM) 	|
| WORKS\_ON 	|    +   	|    -   	|   + (ESSN, PNUM)  	|

# Implementation

## Assertion

> A boolean-valued SQL expression that must be true all time

__Syntax__:
```SQL
CREATE ASSERTION <Assertion_name> CHECK (<Conditions>)
DROP ASSERTION
```

__Example:__
_"The birthday of a manager is smaller than his/her starting date"_

```SQL
CREATE ASSERTION R12 CHECK (
    NOT EXISTS (
        SELECT *
        FROM EMPLOYEE, DEPARTMENT
        WHERE SSN = MGRSSN AND BIRTHDATE > STARTDATE
    )
)
```

## Trigger

> A series of actions associated with certain events and performed whenever these events arise

__Syntax__
```SQL
CREATE TRIGGER <Trigger_name>
AFTER|BEFORE INSERT|UPDATE|DELETE ON <Table_name>
REFERENCING
    NEW ROW|TABLE AS <Variable_name_1>
    OLD ROW|TABLE AS <Variable_name_2>
FOR EACH ROW| FOR EACH STATEMENT
WHEN (<Condition>)
    <SQL_statements>

DROP TRIGGER <Trigger_name>
```

## Transaction

> A set of statements performing a certain process in a database, such that either all statements are performed successfully or nothing happens.

__Example:__
```SQL
Transaction Insert_competition(s, t)
    Insert t into COMPETITION
    Insert t into COMPETITION
    If one statement fails then
        Rollback transaction
    Else
        Commit transaction
    End if
End transaction
```