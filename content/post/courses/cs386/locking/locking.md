---
title: "Locking"
date: 2019-07-29T15:13:11+07:00
lastmod: 2019-07-29T15:13:11+07:00
description: ""
tags: ['cs386']
---

Locking

<!--more-->


# Enforcing Serializability by Locks

## Locks

It is too hard for the scheduler to know if a action could lead to an inconsistent database state after all active transactions commit or abort. Therefore, a scheduler use a simple test which might forbid some actions that could not by themselves lead to inconsistency. A locking scheduler enforces __conflict-serializability__.

When a scheduler uses __locks__, transactions must __request__ and __release__ locks, in addition to reading and writing database elements. The use of locks must be proper in two senses:

* __Consistency of Transactions__ (structure of transactions)
    * A transaction can only read only read or write an element if it previously _requested a lock_ on that element and _hasn't yet released the lock_.
    * If a transaction locks an element, it must unlock that element later.

*  __Legality of Schedules__(structure of scheduler): No two transactions may have locked the same element without having first released the lock

__Example:__

$$T_1: l\_1(A), r\_1(A); w_1(A), u_1(A); l_1(B); r_1(B); w_1(B), u_1(B)$$

$$T_2: l\_2(A), r\_2(A); w_2(A), u_2(A); l_2(B); r_2(B); w_2(B), u_2(B)$$

The legal schedule of these two transactions.

![](2019-07-29-22-50-56.png)

## Two-Phase Locking

> In every transaction, all locks requests precede all unlock requests.

> __$2PL$__ guarantee that the schedule is __conflict-serializable__, therefore __serializable__.

 2PL Transactions         |  Non-2PL Transactions
:-------------------------:|:-------------------------:
   ![](2019-07-29-23-07-30.png) |    ![](2019-07-29-23-08-43.png)


## Risk of Deadlock

One problem that is not solved by _two-phase locking_ is the potential for deadlocks, where several transactions are forced by the scheduler to wait forever for a lock held by another transaction.

 ![](2019-07-29-23-06-25.png)


# Locking Systems With Several Lock Modes

 The locking scheme above is too simple to be a practical scheme. The main problem is that a transaction $T$ must take a lock on a database element $X$ even if it only wants to read $X$ and not write it. There is no reason why several transactions could not read $X$ at the same time.

## Shared and Exclusive Locks

The locking for writing is "stronger" that the lock for reading.

__Denote:__ 

* $sl_i(X)$ means "$T_i$ requests a shared lock on $X$"
* $xl_i(X)$ means "$T_i$ requests an exclusive lock on $X$"

__Requirements:__

* __Consistency of transactions__: You may not write without holding an exclusive lock, and you may not read without holding some locks. All locks must be followed by an unlock of same element.
    * A read action $r_i(X)$ must be preceded by $sl_i(X)$ or $xl_i(X)$, with no intervening $u_i(X)$

    *  A read action $w_i(X)$ must be preceded by  $xl_i(X)$, with no intervening $u_i(X)$

* __Two-phase locking of transactions:__ Locking must precede unlocking.

* __Legality of schedules:__ An element may either be locked exclusively by one transaction or by several in shared mode, but no both

    * If $xl_i(X)$ appears in a schedule, then there cannot be following $xl_j(X)$ or $sl_j(X)$ for some $j \neq i$, without an intervening $u_i(X)$.

    * If $sl_i(X)$ appears in a schedule, then there cannot be a following $xl_j(X)$, for $j \neq i$, without intervening $u_i(X)$.

The schedule satisfies three requirements above is _conflict-serializable_.

__Example:__

Consider two transaction:

$$T_1: sl_1(A); r_1(A); xl_1(B); r_1(B); w_1(B); u_1(A); u_1(B)$$ 
$$T_2: sl_2(A); r_2(A); sl_2(B); r_2(B); u_2(A); u_2(B)$$ 

 ![](2019-07-30-08-15-15.png)

 The scheduler is conflict-serializable.

 * $T_1$ begins by getting a shared lock on $A$. 
 * Then $T_2$ follows by getting shared locks on both $A$ and $B$. 
 * Now, $T_1$ needs an exclusive lock on $B$, since it will both read and write on $B$. However, it cannot get the exclusive lock because $T_2$ already has a shared lock on $B$. Thus, the scheduler forces $T_1$ to wait.
 * Eventually, $T_2$ releases the lock on $B$. 

## Compatibility Matrices

 A __compability matrix__ has a row and column for each lock mode. The rows correspond to a lock that is already held on an element $X$ by another transaction, and the columns correspond to the mode of a lock on $X$ the requested.

 We can grant the lock in mode $C$ if and only if for every row $R$ such that there is already lock on $X$ in mode $R$ by some other transaction, there is a "Yes" in column $C$.

__Example:__

![](2019-07-30-08-29-42.png)

* The column for $S$ says that we can grant a shared lock on element if the only locks held on that element currently are shared locks.
* The column for $X$ says that we can grant exclusive lock only if there are no other locks held currently.

## Upgrading Locks

We can make lock "friendlier" by let the transaction take the shared lock first when then read value of $X$, then later __upgrade__ to exclusive when the transaction is ready to write $X$.

__Example:__

 ![](2019-07-30-08-37-25.png)

* $T_1$ request a shared lock to read $B$ first, the upgrade to an exclusive lock to write $B$.

__Deadlock__

 ![](2019-07-30-08-39-59.png)

$T_1$ and $T_2$ are both able to get shared locks on $A$. Then, they each try to upgrade to exclusive. bu the the scheduler forces each to wait because the other has a shared lock on $A$.


## Update Locks

To avoid the deadlock problem of _upgrading lock_, we can use __update locks__. An update lock $ul_i(X)$ gives transaction $T_i$ only the privilege to read $X$, not to write $X$. However, only the update lock can be upgraded to a write lock later, a read lock cannot be upgraded. We can grant an update lock on $X$ when there are already shared locks on $X$, but once there is an update lock on $X$, we prevent additional locks of any kind

The update lock looks like a shared lock when we are requesting it and looks like an exclusive lock when we already have it.

__Compatibility Matrix:__

 ![](2019-07-30-08-47-40.png)

__Example:__

 ![](2019-07-30-08-57-01.png)

The problem of deadlock in upgrading lock is solved because when update lock is granted on $T_1$, it will prevent any kind of lock on the same database element. Therefore, $T_2$ must wait $T_1$ finish to get the lock.



