---
title: "Normal Form and Functional Dependency"
date: 2019-07-24T19:39:27+07:00
lastmod: 2019-07-24T19:39:27+07:00
description: ""
tags: ['CS386']
---

<!--more-->

# Informal Design Guidelines

Four informal guidelines to determine the quality of the relation schema design:

* The semantic of attributes in scheme must be clear
* Reducing the redundant information in tuples
* Reducing `NULL` values
* Disallowing the possibility of generating __spurious tuples__

## Clear Semantics

Relation schema needs to be easy to explain. Do not combine attributes from multiple entity types and relationship types into single relation.

## Redundant Information and Update Anomalies
Redundant information results in wasted storage.

Update Anomalies can be classified into __insertion anomalies__, __deletion anomalies__, and __modification anomalies__

### Insertion Anomalies
When we insert a new tuple into relation. The values in redundant attributes need to be included (or `NULL`). The `NULL` values could be prevent the insertion to be success if they are on attributes which are primary key or have unique constraint.

E.g: Consider joining relation `EMP` and `DEPT` into `EMP_DEPT`. When inserting a new employee, we need to include their department's name, .... or leave them `NULL`. In addition, creating a department which does not have any employee yet is not allowed since we need to insert a new tuple with `NULL` in `SSN`.

### Deletion Anomalies

Deleting a tuple can lead to lose information of redundant attributes.

E.g: When deleting last employee of a department in `EMP_DEPT`, we lose information of that department.

### Modification Anomalies

We need to update many tuples if we modified the values in redundant attributes.

E.g: If we update the department name in `EMP_DEPT`, we need to update in all tuple representing the employee of that department.

## `NULL` Values
`NULL` values could lead to wasted storage, misunderstanding information, and unpredictable if `NULL` values involve in comparison and aggregate functions.

## Spurious Tuples

Doing natural join operation could produce tuples that do not exists in the original set. These __spurious tuples__ contain invalid information.

# Functional Dependency

## Definition

Functional Dependency is a constrain between two sets of attributes from the database.

Denote: $X \rightarrow Y$. For any $t_1, t_2$ in $r$, if $t_1[X] = t_2[X]$, then $t_1[Y] = t_2[Y]$.

The values of $X$ component of a tuple uniquely determine the values of $Y$ component.

In a relation $R$, if $X$ is a __candidate key__ of $R$, then $X \rightarrow R$

We denote $F$ the set of functional dependency that specified on a relation $R$.

## Inference Rules
 
__Reflexive__

> $\forall Y \subseteq X, X \rightarrow Y$

__Augmentation__

> If $X \rightarrow Y$ and Z

> Then $XZ \rightarrow YZ$

__Transitive__

> If $X \rightarrow Y$ and $Y \rightarrow Z$

> Then $X \rightarrow Z$

__Pseudo transitive__

> If $ X \rightarrow Y$ and $Y, W \rightarrow Z$

> Then $X, W \rightarrow Z$

__Combining__

> If $X \rightarrow Y$ and $ X \rightarrow Z$

> Then $X \rightarrow YZ$

__Splitting__

> If $X \rightarrow Y$ and $ Z \subseteq Y$

> Then $X \rightarrow Z$

## Complete FD
> Consider $X \rightarrow Y$

> If $\neg \exists X' \subset X$ that

> $$F \equiv F - \\{ X \rightarrow Y \\} \cup \\{X' \rightarrow Y\\} $$

> Then $Y$ depends on $X$ completely.

To prove an FD is complete, we need to prove $F_1$ (original set of FDs) is inferred from $F_2$ (new set of FDs) and the reversed. $F_1 \rightarrow F_2$ is trivial, the problem left is $F_2 \rightarrow F_1$.

## Excessive FD

> $X \rightarrow Y$ is excessive if $ F \equiv F - \\{X \rightarrow Y \\}$

## Finding Key
We cane use graph of functional dependency to find key of relation

### Graph of FD
![](2019-07-26-17-09-47.png)

 ![](2019-07-26-17-13-07.png)


### Source Attributes
All __source__ attributes __must appear__ in key of the relation.

In graph of FD, source attributes only have arcs with tails, not with heads.

### Destination Attributes
All __destination__ attributes __must not appear__ in key of the relation.

In graph of FD, source attributes only have arcs with heads, not with tails.

# Decomposition
Decomposition must preserve:

* Lossless join
* Dependency Preservation

## Lossless join

Using tableau to check lossless join.
The algorithm and example can be found [here](https://drive.google.com/open?id=1ozWusDXkHohguXCK4vW0B-JTAJF9wwbx).

## Dependency Preservation

Non-trivial dependency must be persevered.

$$(\pi\_{R\_1}(F) \cup \dots \cup \pi\_{R\_n}(F))^{+} = F^{+}$$

# Normal Form based on Primary Keys

## Normalization of Relations

__Normalization of data__ is a process of analyzing the relation schema based on $FD$ and primary keys to achieve desirable properties. An unsatisfactory relation is decomposed into smaller satisfactory relations.


__Normal Form__ of a relation refers to the highest __Normal Form__ condition that it meets.

## Practical Use

In practice, highest normal form is not required. We usually normalize relations to $BCNF$, rarely $4NF$.

## Keys and Attributes participating in Keys

__Definition:__ An attribute of a relation schema $R$ is called a __prime attribute__ if it is a member of _some candidate key_ of $R$. Otherwise, it is a __nonprime__ attribute.

## First Normal Form (1NF)

> The domain of an attribute must include only _atomic values_

__Why?__: Easy for querying and modifying data

There are 3 main techniques to achieve $1NF$:

* Decomposes non-1NF relation into 1NF relations. Remove attributes that violate $1NF$ and place them in a separate relations. We usually use this approach.

* Expand the key so that there will be separate tuple for non-atomic attributes. This technique introduces _redundancy_ and rarely used.

* If we know the maximum number of values of _non-atomic_ attribute, we can divide it into many atomic attributes. This solution introduces `NULL` values and is difficult to query. We should avoid this solution.

## Second Normal Form (2NF)

$2NF$ is based on __full functional dependency__.

__Full functional dependency:__ A FD $X \rightarrow Y$ is __full functional dependency__ if removal of any attribute $A$ from $X$ makes that dependency not hold anymore. 

If we can remove some attributes $A$ from $X$ without making the $FD$ not hold, then it is a __partial dependency__

> A relation schema $R$ is in $2NF$ if every nonprime attribute $A$ in $R$ is fully functionally dependent on primary key of $R$.

__Why?:__  Since nonprime attributes are dependent only on part of primary key of $R$, there might be duplicated values on those nonprime attributes. This could result in _update anomalies_ We want to eliminate redundant by separating them into other relations.

The test for 2NF involves testing for FD whose left-hand side attributes are part of the primary key.

Example for 2NF Normalization:

 ![](2019-07-25-16-36-01.png)

## Third Normal Form (3NF)

__3NF__ is based on _transitive dependency_.

__Transitive Dependency:__ A FD $X \rightarrow Y$ is a _transitive dependency_ if there exists a set of attributes $Z$ in $R$ that is neither a candidate key or subset of any key of $R$, and both $X \rightarrow Z$ and $Z \rightarrow Y$ hold.

> A relation schema $R$ is in 3NF if it satisfies 2NF and no nonprime attribute of $R$ is transitively dependent on the primary key.

__Why:__ Similar reason to $2NF$.

__Example:__
 ![](2019-07-25-16-47-04.png)


# Boyce-Codd Normal Form

> A relation schema $R$ is in __BCNF__ if whenever a _nontrivial_ functional dependency $X \rightarrow A$ holds in $R$, then $X$ is a superkey of $R$.

Most relation schema that are in 3NF are also in BCNF. Only if there exists some FD $X \rightarrow A$ that holds in a relation schema $R$ with $X$ not being a superkey and $A$ being a prime attribute.

In general, a relation $R$ that is not in $BCNF$ can be decomposed into two relations $R-A$ and $XA$ where $X \rightarrow R$ is the FD that violates $BCNF$.

![](2019-07-26-16-18-13.png)




