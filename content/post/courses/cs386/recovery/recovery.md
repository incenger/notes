---
title: "Recovery"
date: 2019-07-30T21:00:44+07:00
lastmod: 2019-07-30T21:00:44+07:00
description: ""
tags: ['cs386']
---


<!--more-->


# Issues and Models for Resilient Operation

## Failure Modes

* __Erroneous Data Entry__: Could be detected and prevented by using database schema constraints, trigger, etc...

* __Media Failures__: Major of failure of a disk which makes the entire disk unreadable, are generally handled by:
    * Use one of the __RAID__ schemes to restore the lost disk
    * Maintain an __archive__
    * Keep __redundant copies__ of database

* __Catastrophic Failure:__ The database is damaged and lost completely. __RAID__ will not help in this case but __archive__ and __redundant copies__ could be used.

* __System Failures:__ The state of a transaction is  lost (due to power loss, software errors, ...). The principal remedy for system failures is logging of all database changes in a separate, non-volatile log, coupled with recovery when necessary.

## Correct Execution of Transactions

> __The Correctness Principle:__ If a transaction executes in absence of any other transactions of system errors, and it starts with the database in a consistent state, then the database is also in a consistent state when the transaction ends.

> * A transaction is __atomic__, that is, it must be executed as a whole or not at all.

> * We need to control interactions between simultaneously transactions.

# Undo Logging

## Log records

There are several forms of log record that are used:

* `<START T>` indicates that transaction $T$ has begun.

* `<COMMIT T>`: Transaction $T$ has completed successfully and will make no more changes on database elements. Any changes to the database should appear on the disk.

* `<ABORT T>`: Transaction $T$ could not complete successfully. The transaction manager need to make sure that no changes made by transaction $T$ appear on disk.

For __undo log__, we need other kind of log record: __Update record__ - a triple `<T, X, v>` means that transaction $T$ has changed database element $X$, and its former value was $v$.

## The Undo-Logging Rules

There are two rules that transactions must obey in order that an undo log allows us to recover from system failure:

> If transaction $T$ modifies database element $X$, then the log record of the form `<T, X, v>` must be written before the new value of $X$ is written to disk

> If a transaction commits, then its `COMMIT` log record must be written only after all database elements changed by the transaction have been written to disk.

To summarize, one transaction must be written to disk in following order:

* The log records indicating changed database elements

* The changed database elements themselves

* The `COMMIT` log record.

In order to force log records to disk, the log manager needs a `flush log` command that tells buffer manager to copy to disk any log blocks that have not previously been copied to disk or that have been changed since last copied.

__Example:__

![](2019-07-30-22-39-08.png)

$A$ and $B$ cannot be copied to disk until the log records for the changes are on disk. Thus, we need to call `flush log` to assure that those records are on disk.

When it is possible to commit $T$, and the `<COMMIT T>` record is written to the log, we need to `flush log` again to make sure that this record appears on disk.

## Recovery Using Undo Logging

The recovery manager must scan the log from the end. As it travels, it remembers all those transactions $T$ for which it has seen a `<COMMIT T>` record or and `<ABORT T>` record. If it sees a record `<T, X, v>`, then:

* If $T$ is a transaction whose `COMMIT` record has been seen, then do nothing.

* Otherwise, $T$ is an incomplete transaction, or an aborted transaction. The recovery manager must change the value of $X$ in the database to $v$.

After making these changes, the recovery manager must write a log record `<ABORT T>` for each incomplete transaction $T$ that was not previously aborted, and then flush the log.

__Example:__

Consider the sequence of actions from section above. We consider different times where system crash could occur:

* The crash occurs after step (12). In this case, we know that the `<COMMIT T>` record got to the disk before the crash. When we recover, we do not undo the results of $T$.

* The crash occurs between step (11) and (12). It is possible that the log record containing `COMMIT` got flushed to the disk. If so, then the recovery is the same as in case (1). However, if the `COMMIT` recover cannot reach the disk, then we $T$ is an incomplete transaction. When the transaction manager scans the log backward, it comes first to the record `<T, B, 8>`, it therefore stores 8 as value of $B$ on disk. It then comes to the record `<T, A, 8>` and makes $A$ have value 8 on disk. Finally, the record `<ABORT T>` is written to the log and the log is flushed.

* The crash occurs between steps (10) and (11). The `COMMIT` record surely was not written, so $T$ is incomplete and is undone as case (2).

* The crash occurs between steps (8) and (10) $\rightarrow$ similar to case (3).

* The crash occurs prior to step (8). Now, it is not certain whether any of the log records concerning $T$ have reached the disk. However, it doesn't matter, because we know by rule 1 that if the change to $A$ or $B$ reached the disk, then the corresponding log record reached the disk, and it will cause the recovery manager to undo these changes. 

## Checkpointing

When logging follows the undo style, once a transaction has its `COMMIT` log record written to disk, the log records of that transaction are no longer needed during recovery. However, we cannot delete the log prior to a `COMMIT` because there are often many transactions execute at once.

The simple solution is to __checkpoint__ the log periodically. In simple checkpoint, we:

> * Stop accepting new transactions

> * Wait until all currently active transactions commit or abort and have written a `COMMIT` or `ABORT` record on the log.

> * Flush the log to disk

> * Write the log record `<CKPT>`, and flush the log again.

> * Resume accepting transactions.

__Example:__

 ![](2019-07-31-17-16-43.png)

## Nonquiescent Checkpointing

 The problem with simple checkpoint is that it prevents new transactions while checkpoiting is made. Thus, __nonquiescent checkpointing__, which allows new transactions to enter the system during the checkpoint, is preferred.

 > * Write a log record `<START CKPT(T_1, T_2, ... , T_k)>` and flush the log. $T_1, T_2, \dots, T_k$ are __active__ transactions (i.e., transactions that have not yet been committed and written their changes to disk).

 > * Wait until all of $T_1, T_2, \dots, T_k$ commit or abort, but do not prohibit other transactions from starting.

 > * When all of $T_1, T_2, \dots, T_k$ have completed, write a log record `<END CKPT>` and flush the log

 When scanning backward, we may first meet and `<END CKPT>` record or a `<START CKPT(T_1, T_2, ... , T_k)>` record.

 * If we first meet and `<END CKPT>` record, then we know all incomplete transactions must began after the previous `<START CKPT(T_1, T_2, ... , T_k)>` record (active transactions before `<START CKPT>` had committed or aborted before `<END CKPT>`). We may thus scan backward as far as the next `START CKPT`, and then stop.

 * If we first meet a record `<START CKPT(T_1, T_2, ... , T_k)>`, the the crash occurred during the checkpoint. However, the only incomplete transactions are those we met scanning backwards before we reached the `START CKPT` and those of $T_1, T_2, \dots, T_k$ that did not complete before the crash. Thus, we only need to scan until the earliest of these incomplete transactions.

__Example:__

* Case 1:

 ![](2019-07-31-18-05-28.png)

* Case 2:
  ![](2019-07-31-18-06-11.png)

# Redo Logging

The principal differences between __redo__ and __undo__:

> * While __undo logging__ cancels the effect of incomplete transactions and ignores committed ones during recovery, __redo logging__ ignores incomplete transactions and repeats the changes made by committed transactions.

> * While __undo logging__ requires us to write changed database elements to disk before `COMMIT` log record reaches disk, __redo logging__ requires that the `COMMIT` record appear on disk before any changed values reach disk.

## Redo-Logging Rule

In __redo logging__, a log record `<T, X, v>` means "transaction $T$ wrote new value $v$ for database element $X$". Every time a transaction $T$ modifies a database element $X$, a record of the form `<T, X, v>` must be written to the log.

__Write-ahead logging rule:__

> Before modifying any database element $X$ on disk, it is necessary that all log records of this modification, including both update record `<T, X, v>` and the commit `<COMMIT T>` record, must appear on disk.

The order of material associated with one transaction:

* The log records indicating changed database elements.

* The `COMMIT` log record

* The changed database elements themselves.

| Redo | Undo |
| -----|------|
|  ![](2019-07-31-19-15-04.png)|![](2019-07-30-22-39-08.png) |

## Recovery with Redo Logging

To recover using __redo log__:

> * Identify the committed transactions

> * Scan the log forward from the beginning. For each log record `<T, X, v>` encountered:
>   * If T is not a committed transaction, do nothing

>   * If T is committed, write value $v$ for database element $X$.

> * For each incomplete transaction $T$, write an `<ABORT T>` record to the log and flush the log


__Example:__

Consider the log

 ![](2019-07-31-19-15-04.png)

 * If the crash occurs any time after step (9), $T$ is a committed transaction. When scanning the log forward, the log records `<T, A, 16>` and `<T, B, 16>` cause recovery manager to write values 16 for $A$ and $B$.

 * If the crash occurs between steps (8) and (9). If the record `<COMMIT T>` has gotten to the disk, then the recovery proceeds as in case (1), otherwise as in case(3)

 * If the crash occurs prior to step (8), $T$ is an uncompleted transaction, therefore there are nothing to be done and eventually an `<ABORT T>` record is written to the log.

## Checkpointing a Redo Log

The database changes made by a committed transaction can be copied to disk much later than the time at which transaction commits, we cannot limit our concern to transactions that are active at the time we decide to create a checkpoint.

On the other hand, we can complete the checkpoint without waiting for the active transactions to commit or abort since they are not allowed to write to disk at that time anyway.

The steps to perform nonquiescent checkpoint of redo log:

> * Write a log record `<START CKPT(T_1, T_2, ..., T_k)>` where $T_1, T_2, \dots, T_k$ are all active (uncommitted) transaction and flush the log.

> * Write to disk all database elements that were written to buffers but not yet to disk by transactions that had already committed when `START CKPT` record was written to the log.

> * Write an `<END CKPT>` record to the log and flush the log.

 ![](2019-08-01-08-26-58.png)

## Recovery With a Checkpointed Redo Log

* Suppose the last checkpoint record on the log before a crash is `<END CKPT>` $\rightarrow$ every value written by a transaction that committed before the corresponding `<START CKPT(T_1, ..., T_k)>` has had its changes written to disk, so we need not concern ourselves with recovering the effects of these transactions. Thus, we can limit our attention to transactions that are either one of $T_i$ or that started after that log record appeared in the log.

* Suppose that the last checkpoint record on the log is a `<START CKPT(T_1, ..., T_k>` record. We cannot be sure that committed transactions prior to the start of this checkpoint had their changes written to disk. Thus, we must search back to the previous `<END CKPT>` record, find its matching `<START CKPT(S_1, ..., S_m)>` and redo all those committed transactions.

__Example:__

Consider the log 
 ![](2019-08-01-08-26-58.png)

* If a crash occurs at the end, we search backwards, finding the `<END CKPT>` record. We thus know our candidate committed set is $\\{T_2, T_3 \\}$. We search the log as far back as the `<START T_2>` record and redo update records `<T_2, B, 10>`, `<T_2, C, 15>`, and `<T_3, D, 20>`.

* Suppose the crash occurred between the records `<COMMIT T_2>` and `<COMMIT T_3>`. The recovery is similar to the above, except that $T_3$ is longer a committed transaction. Thus, its changes must _not be redone_. Also, we write an `<ABORT T_3>` record to the log after recovery.

* Suppose that the crash occurs just prior to the `<END CKPT>`. We must search back to the next-to-last `START CKPT` record and get list of active transactions (in this case, beginning of the log). We identified that $T_1$ as the only committed transaction

# Undo/Redo Logging

* Undo logging requires that data be written to disk immediately after a transaction finishes, perhaps increasing the number of disk's I/O.

* Redo logging requires us to keep all modified blocks in buffers until the transaction commits and the log records have been flushed, perhaps increasing the average number of buffers required by transactions.

* Both undo and redo logs may put contradictory requirements on how buffers are handled during a checkpoint.

 ![](2019-08-01-09-07-12.png)

## Undo/Redo Rules

The __update__ log record has four components. Record `<T, X, v, w>` means that transaction $T$ changed value of database element $X$, its former value was $v$ and its new value is $w$.

The rule for undo/redo logging:

> Before modifying any database element $X$ on disk because of changes made by some transaction $T$, it is necessary that the update record `<T, X, v,w>` appear on the disk.

__Example:__

 ![](2019-08-01-09-10-48.png)

Step (10) could appear before step (8) or step (9), or after step (11).

## Recovery with Undo/Redo Logging

> * Redo all the committed transactions in the order earliest-first

> * Undo all the incomplete transactions in the order latest-first.

__Example__

 ![](2019-08-01-09-10-48.png)

* Suppose the crash occurs after the `<COMMIT T>` record is flushed to disk. The $T$ is identified at a committed transaction. We write the value 16 for both $A$ and $B$ to disk.

* Suppose the crash occurs before the `<COMMIT T>`, $T$ is treated as an incomplete transaction. Then we need to undo $A$. In this case, $B$ does not need to be undone because its changes have no reached the disk yet.

## Checkpointing an Undo/Redo Log

> * Write a `<START CKPT(T_1, T_2, ..., T_k)>` record to the log, where $T_1, T_2, \dots, T_k$ are all the active transactions, and flush the log.

> * Write to disk all buffers that are _dirty_ (containing changed database elements) of __ALL__ active transactions at `<start ckpt>`. 

> * Write an `<END CKPT>` record to the log and flush the log.

__Example:__

 ![](2019-08-01-12-40-30.png)

* $T_2$ is identified as the only active transaction when the checkpoint begins. Since this log is an undo/redo log, it is possible that $T_2$'s new $B$ value (10) has been written to disk. Since we flush all dirty buffers, $A$ and $B$ would be written to the disk no matter they have already on the disk or not.

* If the crash occurs at the end of this sequence of events, $T_2$ and $T_3$ are identified as committed transactions. Since we find `<END CKPT>` and $T_1$ is prior to the checkpoint, then $T_1$ must have its changes written to the disk. Therefore, we need to redo $T_2$ and $T_3$. When redoing $T_2$, we do not need to look prior to the `<START CKPT>` record because we know that $T_2$'s changes prior to the start of the checkpoint were flushed to disk during the checkpoint.

* Suppose the crash occurs just before the `<COMMIT T_3>` record is written to disk. Then $T_2$ is committed and $T_3$ is incomplete. We redo $T_2$ by setting $C$ to 15 on disk. Then we need to undo $T_3$, that is setting $D$ to 19 on disk.
