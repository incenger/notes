---
title: "Introduction to AI"
date: 2019-10-08T07:48:25+07:00
draft: true
tags: ['cs420']
---
Lecture 1
<!--more-->

# What is AI?

![](../img/2019-10-08-19-17-42.png)

## Acting Humanly: The Turing test approach

The **Turing Test** was designed  to provide a satisfactory operational definition of intelligence. The computer needs to possess the capabilities:

* **NLP** to enable it to communicate successfully
* **Knowledge representation** to store what it perceives.
* **Automated reasoning** to use stored information to answer questions and draw conclusions.
* **Machine learning** to adapt new circumstances and detect and extrapolate patterns.

Variations of original Turing Test:

* Reversed Turing Test: CAPTCHA
* Total Turing Test: include perceptual (computer vision) and objects manipulation (robotics) abilities

## Thinking Humanly: The cognitive modelling approach

If we want a computer to think like human, we need to have some ways to determine how humans think. There are some ways to do this

* Introspection: Trying to catch our thoughts
* Psychological Experiment: Observing humans
* Brain imaging: observing brain
  
## Think Rationally: The "law of thoughts" approach

By defining "right thinking" as irrefutable reasoning processes and use logic notation for statements about objects and their relationship in the world.

## Act Rationally: The rational agent approach

**Our main focus**

A rational agent acts to achieve the best expected outcomes given available information.

The main advantages:

* More general than "law of thoughts" because correct inference is just one way to achieve rationality
* More amenable

## History and Foundation of AI

**See in TextBook**
