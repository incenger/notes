---
title: "Intelligent Agent"
date: 2019-10-08T19:38:14+07:00
draft: true
tags: ['cs420']
---



<!--more-->


# Agents and Environments

> An agent is anything that can perceives its environments through sensors and acts upon through actuators.

A human, robotic or software could be an agent.

> Percept: agent's perceptual input at any given instant
> 
> Percept sequence: complete history of everything agents has perceived.

Mathematically speaking, agent's behaviors is described by **agent function** that maps any given **percept sequence** to an action. Agent function will be implemented by an **agent program**.


# Good behaviors: The concept of Rationality

We use **performance measure** that evaluate any given sequence of environment states caused by sequence of agent's actions.

As a general rule, we should design performance measures according to what we wants in the environment, not what we thinks the agent should behave.

## Rationality

> For each possible percept sequence, a rational agent should select an action that is expected to maximize its performance measure, given the evidence provided by the percept sequence and whatever built-in knowledge the agent has.

An agent could be rational under one circumstance but irrational under other circumstances.

## Omniscience, learning, and autonomy

### Omniscience

**Rationality $\neq$ Omniscience**

An omniscience agent knows actual outcome of its actions and act accordingly, but omniscience is impossible in reality.

We also need to ensure that we do not allow agent to take "silly" actions. The agent should gather information in order to modify future percepts (**information gathering**).

### Learning

The agent should learn as much as possible from what it perceives and may modify and augment its initial configuration.


### Autonomy

A rational agent should be autonomous - learn what it can to compensate for partial or incorrect prior knowledge

# Nature of Environment

## Specifying task environment

**PEAS** (Performance, Environment, Actuators, Sensors)


![](../img/2019-10-08-20-43-43.png)

## Property of task environment
| Dimension         	|                      	|
|-------------------	|----------------------	|
| Fully observable  	| Partially observable 	|
| Single agent      	| Multiagent           	|
| Deterministic     	| Stochastic           	|
| Episodic          	| Sequential           	|
| Static            	| Dynamic              	|
| Discrete          	| Continuous           	|
| Known             	| Unknown              	|

* **Fully Observable** and **Partial Observable** and **Unobservable**: Whether the agent's sensors give it access to the complete state of environment
* **Single agent** and **multiagent**: $B$ is an agent if its behavior is best described as maximizing a performance measure whose value depends on agent A's actions. We have competitive and cooperative multiagent.
* **Deterministic** and **Stochastic**: Whether the next state of the environment is completely determined by the current state and the action executed by the agent. Usually when the environment is partial observable, uncertainty arises, and therefore stochastic.
* **Episodic** and **Sequential**: In episodic task environment, agent's experience is divided into atomic episode. Action performed in one episode is irrelevant to next episodes.
* **Static** and **Dynamic**: Whether the environment can change when the agent is deliberating.
* **Discrete** and **Continuous**: State of environment, the way time handled, and the percepts and actions of the agents.
* **Known** and **Unknown**: Refers to the agent's knowledge of the nature of the environment.

# Structure of Agent

## The agent architecture and agent program

> Agent = architecture + program

Architecture is some sort of computing device with physical sensors and actuators that the program runs on. The program has to be appropriate for the architecture

We can implement the agent program as look-up table, however, the size of look-up table skyrockets as the size of the set of possible percepts increases.

## Types of agent program

### Simple reflex agents

The agent select actions based on the current percept, ignoring the rest of the percept history.

> If `current_percept` then `action`

![](../img/2019-10-22-08-04-12.png)

```pseudocode
function SIMPLE-REFLEX-AGENT(percept) returns an action
    persistent: rules, a set of condition–action rules
    
    state <- INTERPRET-INPUT(percept)
    rule <- RULE-MATCH(state, rules)
    action <- rule.ACTION
    return action
```
### Model-based reflex agent

The agents keeps internal state and updates it as time goes by encoding the information about how the world evolves and how the action of the agent affects the world.

### Goal-based agent

The agent needs some sort of goal information that describes situations that are desirable.

### Utility-based agent

The agent need to  measure the degree of success.
