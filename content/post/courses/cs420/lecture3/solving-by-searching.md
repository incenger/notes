---
title: "Solving by Searching"
date: 2019-10-22T08:24:18+07:00
draft: true
tags: ['cs420']
---

<!--more-->

# Problem-solving agents

## Well-defined problems and solutions

A problem can be defined formally by five components:

* Possible **states** of the agent.
* The **initial state** that the agent starts in.
* A description of possible **actions** available to the agent. Given a state $s$, $ACTIONS(s)$ return the sections of possible actions that can be executed in $s$.
* A description of what each action does (**transition model**). $RESULT(s, a)$ returns the state the results from doing action $a$ at state $s$.
* The **goal test**, which determines whether a given state is a goal state.
* A **path cost** function that assigns a numeric cost to each path.

A **solution** to a problem is an action sequence that leads from the initial state to a goal state. 
Solution quality is measured by the  path cost function, and an **optimal solution** has the lowest path cost among all solutions.

## Abstraction

The process of removing irrelevant detail from a representation
is called **abstraction**. We need to abstracting both states and actions.

The abstraction is **valid** if we can expand any abstract solution into a solution in the more detailed world.

The abstraction is **useful** if carrying out each of the actions in the solution is easier than the original problem.
# Searching for solutions

## Infrastructure for search algorithms
Search algorithms require a data structure to keep track of the search tree that is being constructed. 
For each node $n$ of the tree, we have a structure that contains four components:

*  $n.STATE$: the state in the state space to which the node corresponds.
*  $n.PARENT$: the node in the search tree that generated this node
*  $n.ACTION$: the action that was applied to the parent to generate the node
*  $n.PATH-COST$: the cost, traditionally denoted by $g(n)$, of the path from the initial state to the node, as indicated by the parent pointers.

![](../img/2019-10-22-21-55-15.png)

The data structure to store the node is **queue** (FIFO, LIFO, priority queue). The operations on a queue are:

* $EMPTY?(queue)$ returns true only if there are no more elements in the queue.
* $POP(queue)$ removes the first element of the queue and returns it.
* $INSERT(element, queue)$ inserts an element and returns the resulting queue.

## Measuring problem-solving performance

We can evaluate an algorithm's performance in 4 ways:

* **Completeness**: Is the algorithm guaranteed to find a solution when there is one?
* **Optimality**: Does the strategy find the optimal solution.
* **Time complexity**: How long does it take to find a solution?
* **Space complexity**: How much memory is needed to perform the search?

Complexity is expressed in terms of three quantities:

* $b$, the **branching factor** or maximum number of successors of any node.
* $d$, the **depth** of the shallowest goal node.
* $m$, the **maximum length** of any path in the state space

# Uninformed search strategies

The term means that the strategies have no additional information about states beyond that provided in the problem definition. 
All they can do is generate successors and distinguish a goal state from a non-goal state. 
All search strategies are distinguished by the *order* in which nodes are expanded.

![](../img/2019-10-22-23-08-43.png)

## Breadth-first search

![](../img/2019-10-22-22-05-18.png)


BFS is **complete** given the branching factor $b$ is finite and the shallowest goal node is at some finite depth $d$.

BFS finds the *shallowest* goal node, however, it is not necessarily the optimal one. BSF is optimal if the path cost is a nondecreasing function of the depth of the node. The most common such scenario is that all actions have the same cost.

Suppose every state has $b$ successors and the solution is at depth $d$, in the worst case (it is the last node generated at level $d$), the total number of generated node is:
$$b + b^2 + b^3 + \dots + b^d = O(b^d)$$

For BFS, every node generated remains in memory (in *explored set* or *frontier*). There will be $O(b^{d-1})$ nodes in *explored set* and $O(b^{d})$ nodes in *frontier*. So, the **space complexity** is $O(b^d)$, dominated by the size of the frontier.

> The memory requirements are a bigger problem for breadth-first search than is the execution time.

> Exponential-complexity search problems cannot be solved by uninformed methods for any but the smallest instances. 

## Uniform-cost search

![](../img/2019-10-22-22-47-24.png)

There are two significant differences between UCS and BFS

* The first is that the goal test is applied to a node when
it is **selected for expansion** rather than when it is first generated. 
* The second difference is that a test is added in case a better
path is found to a node currently on the frontier.


UCS will get stuck in an infinite loop if there is a path with an infinite sequence of zero-cost actions. Completeness is guaranteed provided the cost of every step exceeds some small positive constant $\epsilon$.

UCS is optimal in general since it expands nodes in order of their optimal path cost.

UCS is guided by path cost rather than depths, so its complexity is not easily characterized in terms of $b$ and $d$. Let $C^{\*}$ be the cost of optimal solution and assume every action costs at least $\epsilon$. Then the algorithm worst-case time and space complexity is  $O(b^{1 + \lfloor \frac{C^{\*}}{\epsilon} \rfloor})$, which can be much greater than $O(b^d)$. ($ 1+ \frac{C^{*}}{\epsilon}$ is approximate the number of states on the optimal path.)

## Depth-first search

**Depth-first search** always expands the deepest node in the current frontier of the search tree. As an alternative to the $GRAPH-SEARCH$-style implementation, it is common to implement depth-first search with a recursive function that calls itself on each of its children in turn.

The properties of depth-first search depend strongly on whether the graph-search or tree-search version is used.

The graph version is complete in finite state spaces because it will eventually expand every node. The tree-search version, however, is not complete. $DFS$ tree search can be modified at no extra memory cost so that it checks new states against those on the path from the root to the current node. This avoids infinite loops in finite state spaces but does not avoid the proliferation of redundant paths.

$DFS$ is not optimal because will explore the "left-most" goal node.

The time complexity of $DFS$ graph search is bounded by the size of the state space. A $DFS$ tree search may generate all of the $O(b^m)$ nodes in the search tree. This is terrible if $m$ is larger than $d$.

For $DFS$ graph search, there is no advantage in space complexity over $BFS$. However, a $DFS$ tree search needs to store only a single path from the root to a leaf node, along with the remaining unexpanded sibling nodes for each node on the path. Therefore, it requires storage of only $O(bm)$ nodes.

## Depth-limited search

**Depth-limited search** treats nodes at depth $l$ as if they have no successors. 

![](../img/2019-10-25-22-45-41.png)

$DLS$ is not complete if we choose $l < d$. It is also not optimal if we choose $l > d$. The time complexity and space complexity are $O(b^l)$ and $O(bl)$.

## Iterative deepening DFS

Iterative deepening search gradually increases the limit $l$ —first 0, then 1, then 2, and so on — until a goal is found.

![](../img/2019-10-25-22-50-05.png)

Like $DFS$, its space complexity is $O(bd)$. Like $BFS$, it is complete when the branching factor $b$ is finite and optimal when the path cost is a nondecreasing function of the depth of the node.

In an $IDS$, the nodes on the bottom level (depth $d$) are generated once, those on the next-to-bottom level are generated twice, and so on, up to the children of the root, which are generated $d$ times. So the total number of nodes generated in the worst case is :

$$db + (d-1)b^2 + \dots b^d  = O(b^d)$$

> In general, iterative deepening is the preferred uninformed search method when the search space is large and the depth of the solution is not known.

## Bidirectional Search

We run two simultaneous searches from the initial state and from the goal state, hoping that two searches will meet in the middle. Bidirectional search is implemented by replacing the goal test with a check to see whether two frontiers of the two searches intersect.

The time complexity of bidirectional search using breadth-first searches in both direction is $O(b^{d/2})$. The space complexity is also $O(b^{d/2})$

The bidirectional search using BFS in both directions is complete if $b$ is finite and is optimal if step costs are all identical

The main challenge in using bidirectional search is that computing predecessors for searching backward.

![Comparing uniformed search strategies](../img/2019-10-29-16-42-11.png)

# Informed Search Strategy

**Informed** - using problem-specific knowledge beyond the definition of the problem itself.

The general approach is called **best-first search** - an instance of general $TREE-SEARCH$ and $GRAPH-SEARCH$ in which  a node is selected for expansion based on an evaluation function $f(n)$. The node with lowest $f(n)$ is expanded first.

Most best-first search algorithms include a component of $f$ a **heuristic function** $h(n)$ -  estimated cost of the cheapest path from the state at node $n$ to a goal state. Unlike $g(n)$, depends only the state of that node.



## Greedy best-first search

Greedy best-first search evaluates nodes by using just the heuristic function $f(n) = h(n)$. At each step, it tries to get close to the goal as it can.

Greedy best-first search ($TREE-SEARCH$ version) is incomplete even in finite state spaces, the $GRAPH-SEARCH$ version is complete in finite spaces.

In worse case, time and space complexity for the $TREE-SEARCH$ version is $O(b^m)$.

## $A^{*} search$

$A^{*}$ evaluates node by combining $g(n)$, the cost to reach the node, and $h(n)$, the estimated cost to get to the goal:

$$f(n) = h(n) + g(n)$$

If the heuristic function $h(n)$ satisfies certain conditions, $A^{*}$ search is both complete and optimal. The algorithm is identical to $UCS$ expect it uses $g + h$ instead of $g$.

### Conditions for optimality

The first condition we require for optimality is that $h(n)$ be an **admissible heuristic**. 

> An admissible heuristic is one that never **overestimates** the cost to reach the goal.

A second, slightly stronger condition called **consistency** (or sometimes monotonicity) is required only for applications of $A^{*}$ to graph search. 

> A heuristic $h(n)$ is consistent if, for every node $n$ and every successor $n'$ of $n$ generated by any action $a$, the estimated cost of reaching the goal from $n$ is no greater than the step cost of getting to $n'$ plus the estimated cost of reaching the goal from $n'$
> $$h(n) \leq c(n, a, n') + h(n')$$
> Every consistent heuristic is also admissible.

### Optimality for $A^{*}$

> The tree-search version of $A^*$ is optimal if $h(n)$ is admissible, while the graph-search version is optimal if $h(n)$ is consistent.

**Proof:** Textbook

If $C^{\*}$ is the cost of the optimal solution path, $A^{\*}$ will expands all nodes with $f(n) < C^{\*}$ and might expand some of nodes which have $f(n) = C^{\*}$ before expand the goal node.

*Completeness* requires that there be only finitely many nodes with cost less than or equal to $C^*$, a condition that is true if all step costs exceed some finite $\epsilon$ and if $b$ is finite.

Computation time is $A^\*$'s main drawback. Becasue it keeps all generated nodes in memory, $A^*$ often runs out of space long before it runs out of time.

## Memory-bounded heuristic search

### ID$A^\*$

The simplest way to reduce memory requirements for $A^\*$ is to adapt idea of *iterative deepening* to heuristic context, resulting in the **iterative-deepening $A^*$**. The main difference is that the cutoff used is the $f$-cost: at each iteration , the the cutoff value is the smallest $f$-cost of any node that exceeded the cutoff on the previous iteration.

### Recursive best-first search (RBFS)

RBFS's structure is similar to that of DFS, but rather than continuing indefinitely down the current path, it uses the $f\\_limit$ variable to keep track of $f-$value of the best *alterative* path available from any ancestor of the current path. If the current node exceeds this limit, the recursion unwinds back to the alternative path. As the recursion unwind, RBFS replaces the $f$-value of each node along the path with **backed-up value** - the best $f$-value of its children. In this way, RBFS remembers the $f$-value of the best leaf in the forgotten  subtree and can therefore decide whether it's worth reexpanding the subtree at some later time.

|![](../img/2019-11-03-15-47-28.png)|
|:----------------------------------:|
|Recursive best-first search algorithm|

Like $A^*$ tree search, RBFS is an *optimal* algorithm if the heuristic function $h(n)$ is admissible. Its space complexity is linear in the depth of the deepest solution. However, its time complexity is difficult to characterize: it depends on the accuracy of the heuristic function and how often the best path changes as nodes expanded.

Both ID$A^\*$ and $RBFS$ suffers from using *too* little memory. 

### Simplified Memory Bounded $A^*$

SM$A^\*$ proceeds similarly to $A^\*$. When the memory is full, it drops the *worst* leaf node (highest $f$-value). 

SM$A^\*$ is complete if there is any reachable solution. It returns the best reachable solution, therefore it is optimal if any optimal solution is reachable.

# Heuristic function

## The effect of heuristic accuracy on performance

One way to characterize the quality of a heuristic is the effective branching factor $b^\*$. If the total number of nodes generated by $A^\*$ for a particular problem is $N$ and the solution depth is $d$, then $b^\*$ is the branching factor that a uniform tree of depth $d$ would have to have in order to contain $N+1$ nodes. Thus, 

$$N + 1  = 1 + b^\* + (b^\*)^2 + \dots + (b^\*)^d$$

A well-designed heuristic would have a value of $b^
\*$ close to  $1$.

We say $h_2(n)$ dominates $h_1(n)$ if for any node $n$, $h_2(n) \geq h_1(n)$. Dominance means that $A^\*$ using $h_2$ never expand more nodes than that using $h_1$ (except for some nodes with $f(n) = C^\*$).

## Generating admissible heuristic from relaxed problems

A problem with fewer restrictions on the actions is called a **relaxed problem**.

> The cost of an optimal solution to a relaxed problem is an an admissible heuristic for the original problem. Furthermore, it is consistent.

## Generating admissible heuristics from subproblems: Pattern database

Admissible heuristics can also be derived from the solution cost of a **subproblem** of a given problem.

![](../img/2019-11-03-17-10-00.png)

The idea behind **pattern databases** is to store these exact solution costs for every possible subproblem instance.

Then we compute an admissible heuristic $h_{DB}$ for each complete state encountered during a search simply by
looking up the corresponding subproblem configuration in the database.

For 8-puzzle problems, if we only count number of moves that involves with tiles that we are generating subprobems. We can actually add two cost (for example, add costs for subproblems 1-2-3-4 and 5-6-7-8 together.) That is the idea behind **disjoint pattern databases**.

## Learning heuristics from experience

We can provide examples with states from solution path and the actual cost. From these examples, learning algorithms can be used to construct $h(n)$ that can predict solution cost from other states arise during search. These learning methods work best when supplied with features of a state that are relevant to predicting state's value.



