---
title: "Adversarial Search"
date: 2019-11-05T10:00:23+07:00
draft: true
tags: ['cs420']
---

In which we examine the problems that arise when we try to plan ahead in a world where other agents are planning against us.

- [ ] Implement alpha-beta pruning
- [ ] 

<!--more-->

# Game

A game can be formally defined as a kind of search problem with the following elements:

* $S_0$ - The **initial state**.
* $PLAYERS(s)$ - Defines which player has the move in a state
* $ACTION(s)$ - returns the set of legal moves in a state.
* $RESULT(s, a)$ - The **transition model**, which defines the result of a move
* $TERMINAL-TEST(s)$ - A **terminal test**, returns `TRUE` when the game is over and false, otherwise.
* $UTILITY(s, p)$ - A **utility function** defines the final numeric value for a game that ends in terminal state $s$ for a player $p$.

# Optimal Decisions in Games

Given a game tree, the optimal strategy can be determined from the **minimax value** of each node: $MINIMAX(n)$. The minimax value of a node is the utility (for $MAX$) of being in correct state, assuming both players play optimally. 

![](../img/2019-11-05-10-24-56.png)

## The minimax algorithm

The **minimax** algorithm computes the minimax decision from the current state. The recursion proceeds all the way down to the leaf nodes, the minimax values are backed up as the recursion unwinds.

![](../img/2019-11-05-10-42-49.png)

## Optimal decisions in multiplayer games

First, we need to replace the single value for each node with a *vector* of values. For terminal states, this vector gives utility of the state from each player's perspective.

![](../img/2019-11-06-09-43-35.png)

Multiplayer games usually involve **alliances** among the players. It can be made and broken as the game proceeds.

# Alpha-Beta Pruning

The problem with minimax is that the number of states it has to examine is exponential with depth of the tree. We borrow the idea of **pruning** to eliminate large parts of the tree form consideration.

![](../img/2019-11-05-11-01-16.png)

The general principle is this: consider a node $n$ somewhere in the tree, such that Player has a choice of moving to that node. If Player has a better choice $m$ either at parent node of $n$ or any choice point further up, then $n$ will never be reached in actual play.

Alpha-beta pruning defines two parameters that describe bounds on backed-up values that appear anywhere along the path:

* $\alpha = $ the value of the best (i.e., highest-value) choice we have found so far at any choice point along the path for $MAX$.
* $\beta = $ the value of the best (i.e, lowest-value) choice we have found so far at any choice point along the path for $MIN$.

[Example](http://web.cs.ucla.edu/~rosen/161/notes/alphabeta.html)

![](../img/2019-11-06-10-17-18.png)



## Move ordering

The effectiveness of alpha-beta running is highly dependent on the order in which the states are examined.

If we can examine first the successors that are likely to be the best, it turns out that alpha-beta pruning needs to examine only $O(b^{m/2})$ to find the best move. If successors are examined randomly, then the total number of nodes need to be examined is roughly $O(b^{3m/4})$

# Imperfect Real-time Decisions

Alpha-beta pruning has to search all the way to terminal states. This depth is, however, usually not practical.

We can alter minimax or alpha-beta pruning in two ways:

* Replace the utility function by the a **heuristic evaluation function**.
* Replace the terminal test by a **cutoff test**. 

![](../img/2019-11-12-08-10-59.png)

## Evaluation functions

An evaluation function returns an *estimate* of the expected utility at a given state.

The evaluation function should *order* the terminal states in the same way as the true utility function: states that are wins must evaluate better than draws, which in turn must be better than losses. The computation must note take too long.

