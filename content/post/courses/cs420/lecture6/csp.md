---
title: "Constraint Satisfaction Problems"
date: 2019-11-12T08:32:00+07:00
draft: true
tags: ['cs420']
---

> In which we see how treating states as more than just little black boxes leads to the invention of a range of powerful new search methods and a deeper understanding of problem structure and complexity.

<!--more-->

# Defining Constraint Satisfaction Problems

A constraint satisfaction problem consists of three components $X$, $D$, and $C$:

* $X$ is a set of variables.
* $D$ is a set of domains, one for each variable.
* $C$ is set of constraints that specify allowable combinations of values. Each constraint $C_i$ consists of a pair of $\<scope, rel\>$, where $scope$ is a tuple of variables that participate in the constraint and $rel$ is the relation the defines the values that those variables can take.

Each state in **CSP** is defined by an assignment of values to some or all of the variables. An assignment that does not violate any constraints is called **consistent** (or legal). A **complete** assignment is one which every variable is assigned, and a solution to **CSP** is a consistent and complete assignment.

**Why formulate a problem as a CSP?**

CSP yields a natural representation for a wide variety of problems. If you already have a CSP-solving system, it is often easier to solve problem using CSP than design a custom solution using another search technique.

In addition, CSP solvers can be faster than state-space searchers because the CSP solver can quickly eliminate large swatches of the search space. 


## Example problems

### Map coloring

**Textbook**

### Job-shop scheduling

**Textbook**

## Variations on the CSP formalism

### Types of variables

The simplest kind of CSP involves variables that have **discrete, finite domains.** A discrete domain can be **infinite**. With infinite domains, a **constraint language** must be used (such that $T_1 + d \leq T_2$).

Constraint satisfaction problems with **continuous domains** are common in the real world and are widely studied in the field of operations research

### Types of constraints

The simplest type is **unary constraint**, which restricts the value of a single variable.

A **binary constraint** relates two variables. A **binary CSP** is one with only binary constraint; it can be represented as a constraint graph.

A constraint involving an arbitrary number of variables is called **global constraint**.

### Preference constraints

Many real-world CSPs include **preference constraints** indicating which solutions are preferred. 

For example, in a university class-scheduling problem there are absolute constraints that no professor can teach two classes at the same time. But we also may allow preference constraints: Prof. R might prefer teaching in the morning, whereas Prof. N prefers teaching in the afternoon. A schedule that has Prof. R teaching at 2 p.m. would still be an allowable solution (unless Prof. R happens to be the department chair) but would not be an optimal one. 

Preference constraint is often encoded as costs on individual variable assignments. With this formulation, CSP with preferences can be solved with optimization search methods. We call such a problem a **constraint optimization problem**.

# Constraint Propagation: Inference in CSPs

In regular state-space search, an algorithm can only search for the solution. In CSPs, an algorithm can choose to search (choose a new variable assignment from several possibilities) or do a specific type of **inference** called **constraint propagation**: using the constraints to reduce the number of legal values of a variable.

The key idea is **local consistency**. If we treat each variable as a node in a graph and each binary constraint as an arc, then the process of enforcing local consistency in each part of the graph causes inconsistent values to be eliminated throughout the graph. 

## Node consistency

A single variable (a node) is **node-consistent** if all the values in the variable's domain satisfy the variable's unary constraints. The network is node-consistent if every variable in the network is node-consistent. It is always possible to eliminate all the unary constraints in a CSP by running node consistency.

## Arc consistency

A variable in a CSP is **arc-consistent** if every value in its domain satisfies the variable's binary constraints.

> $X\_i$ is arc-consistent with respect to another variable $X\_j$ if for every value in the current domain $D_i$ there is some value in the domain $D_j$ that satisfies the binary constraint on the arc $(X\_i, X\_j)$.

A network is arc-consistent if every variable is arc consistent with every other variable.

The most popular algorithm for arc consistency is called $AC-3$.

![](../img/2019-11-16-12-15-33.png)

* To make every variable arc-consistent, the $AC-3$ algorithm maintains a set of arcs to consider. 
* Initially, the set contains all arcs in the CSP. 
* The algorithm then pops an arbitrary arc $(X_i, X_j)$ from the set and makes $X_i$ arc-consistent with respect to $X_j$
  * If this leaves $D_i$ unchanged, the algorithm moves on to the next arc.
  * If this revises $D_i$ (makes domain smaller), then we add to the set all arcs $(X_k, X_i)$ where $X_k$ is a neighbor of $X_i$. If $D_i$ is revised to nothing, then the whole CSP has no consistent solution. The algorithm immediately returns failure. 

# Backtracking Search for CSPs

Many CSPs cannot be solved by inference alone, there comes a time when we must search for a solution. 

A state would be a partial assignment, and an action would be adding $var = value$ to the assignment. 

Since variable assignments are commutative, we need only consider a *single* variable at each node in the search tree. 

The term **backtracking** search is used for a depth-first search that chooses values for one variable at a time and backtracks when a variable has no legal values left to assign.

![](../img/2019-11-16-13-37-58.png)

## Variable and value ordering

The backtracking algorithm contains
```pseudocode
var <- SELECT-UNASSIGNED-VARIABLE(csp)
```
The simplest strategy for $SELECT-UNASSIGNED-VARIABLE$ is to choose the next unassigned variable in order. 

The intuitive idea - choosing the variable with the fewest "legal" values - is called the **minimum-remaining-values**(MRV) heuristic. 

The $MRV$ heuristics doesn't help at all in choosing the value for the first assignment. In this case, we use the **degree heuristic** - it attempts to reduce the branching factor on future choices by selecting variable that is involved in the largest number of constraints on other unassigned variables.

The $MRV$ is usually a more powerful guide, but the degree heuristic can be useful as a tie-breaker.

The **least constraining value** heuristic prefers the value that rules out the fewest choices for the neighboring variables in the constrain graph.

Why should variable selection be fail-first, but value selection be fail-last? 

* A variable ordering that chooses a variable with the
minimum number of remaining values helps minimize the number of nodes in the search tree by pruning larger parts of the tree earlier. 
* For value ordering, the trick is that we only need one solution; therefore it makes sense to look for the most likely values first. 

## Interleaving search and inference

We can use $AC-3$ and other inference algorithms to infer reduction in the domains of variables before we begin search. However, it is more powerful to infer new domain reductions on neighboring values every time we make a choice of a value for a variable.

One of the simplest form of inference is **forward checking**: When a variable $X$ is assigned, the forward-checking process establishes arc-consistency for it: for each unassigned variable $Y$ that is connected to $X$ by a constraint, we eliminate values of $Y$ that is inconsistent with the value chosen for $X$. There is no reason to do forward checking if we have already done arc consistency as preprocessing step.

# Local search for CSPs

The local search for CSPs use complete-state formulation: the initial state assigns a value to every variable and the search changes the value of one variable at a time. Typically, the initial guess violates several constraints. The point of local search is to eliminate the violated constraints.

## Min-conflicts

The most obvious heuristic for choosing a new value for a variable is the **min-conflicts** - select the value that results in the minimum number of conflicts with other variables.

![](../img/2019-11-16-15-38-20.png)

The landscape of CSP under min-conflicts heuristic usually has a series of plateaux. Plateau search, tabu search or simulated annealing can be used.

## Constraint weighting

Another technique, **constraint weighting** concentrate the search on the important constraints. 

* Each constraint is given a numeric weight, initially all $1$. 
* At each step, the algorithm choose a variable/value pair to change that has the lowest total weight of all violated constraints. 
* The weights of each constraint that is violated is then incremented. This has two benefits: 
  * Adding topography to plateaux, making sure it is possible to improve from the current state.
  * Over time,  adding weights to constraint that are difficult to solve. 

# The Structure of Problems


The only way we can possibly hope to deal with the real world is to decompose it into many subproblems. 

**Independent subproblems** can be ascertained by finding connected components of the constraint graph. Each component corresponds to a subproblem $CSP_i$. The solution for $\bigcup\_i CSP\_i = \bigcup\_iS\_i$ where $S_i$ is the solution for subproblem $i$.

Completely independent subproblems result in linear time complexity, however, they are rare. Some other graph structures are more frequent and also easy to solve.

## Tree-structured CSP

> Any tree-structured CSP can be solved in time linear in the number of variables.

We introduce the new notion of consistency called **directed arc consistency** ($DAC$). A CSP is defined to be $DAC$ under an ordering of variable $X\_1, X\_2, \dots, X\_n$ iff every $X\_i$ is arc-consistent with each $X\_j$ for $j > i$.

**To solve a tree-structured CSP**:

* First we find a topological sort tree.

![](../img/2019-11-16-20-56-59.png)

* Make this tree $DAC$. Since we have total $n-1$ arcs, we can do arc-consistency in $O(n)$ steps, each of which compares up to $d$ possible domain values for two variables. The total time complexity is $O(nd^2)$
* We can just go through the variables and choose remaining values. Since the each link from the parent to its child is arc-consistent, the solution always exists.

![](../img/2019-11-16-21-04-00.png)

## Reducing graph to trees

There are two primary ways to reduce a general graph to trees: removing nodes and collapsing nodes.



