---
title: "Logical Agents"
date: 2019-11-19T08:29:20+07:00
draft: true
tags: ['cs420']
---

> In which we design agents that can form representations of a complex world, use a process of inference to derive new representations about the world, and use these
new representations to deduce what to do.

<!--more-->


# Knowledge-based Agents

The central component of a knowledge-based agent is its **knowledge base** (KB), a set of **sentences**. Each sentence is expressed in **knowledge representation language**. A **axiom** is the sentence that is taken as given without deriving from other sentences.

The standard operations are $TELL$ and $ASK$. Both of them may involve **inference** - deriving new sentences from old.

![](../img/2019-11-19-08-58-38.png)

* $MAKE-PERCEPT-SENTENCE$ constructs a sentence asserting that the agent perceived the given percept.
* $MAKE-ACTION-QUERY$ constructs a sentence that asks what action should be done at the current time
* $MAKE-ACTION-SENTENCE$ constructs a sentence asserting that the chose action was executed.

Knowledge-based agents can be built by **declarative approach**, simply $TELL$ them what they need to know until they can operate in the environment.

In contrast, the **procedural** approach encodes desired behaviors directly from the code. We can combine the two approaches.

We can also provide the agent a mechanism that allows it to learn for itself.

# The Wumpus World

![](../img/2019-11-19-09-12-25.png)

**PEAS** description:

* **Performance Measure**
  * $+1000$ for climbing out of the cave with the gold
  * $-1000$ for falling into the pit or being eaten by the wumpus
  * $-1$ for each action taken
  * $-10$ for using up the arrow
* **Environment**
  * A $4 \times 4$ grid of rooms
  * The agent always starts at $(1, 1)$ and faces right;
  * The locations of gold, wumpus and pit are random (wumpus and pit are not at the starting cell).
* **Actuators**
  * Move forward, turn left, turn right
  * Grab, shoot, and climb
* **Sensors**
  * In the square containing the wumpus and in the directly (not diagonally) adjacent squares, the agent will perceive a $Stench$.
  * In the squares directly adjacent to a pit, the agent will perceive a $Breeze$.
  * In the square where the gold is, the agent will perceive a $Glitter$.
  * When an agent walks into a wall, it will perceive a $Bump$.
  * When the wumpus is killed, it emits a woeful $Scream$ that can be perceived anywhere in the cave.

# Logic

Sentences in the knowledge base are expressed according to the syntax of the representation language.

A logic must define the **semantic** of sentences. The semantics defines the truth of each sentence with respect to each possible word. To be precise, we use the term **model** in place of "possible world". Formally, possible models are just all possible assignments of real numbers to variables. If a sentence $\alpha$ is true in model $m$, we sat they $m$ satisfies $\alpha$ (or $m$ is the model of $\alpha$). $M(\alpha)$ is the set of all models of $\alpha$.

**Logical entailment** is the idea that a sentence follows logically from another sentence (Mathematical notation: $\alpha \models \beta$). 

$$\alpha \models \beta \text{ iff } M(\alpha) \subseteq B(\beta)$$

If an inference algorithm $i$ can derive $\alpha$ from $KB$, we write $KB \vdash_{i} \alpha$.

An inference algorithm derives only entailed sentences is called **sound** (or **truth-preserving**). Soundness is a highly desirable property. The property of **completeness** is also desirable: an inference algorithm is complete if it can derive any entailed sentence.

![](../img/2019-11-25-15-04-26.png)

# Propositional Logic

## Syntax

The **atomic sentences** consist of a single **propositional symbol**. Each such symbol stands for proposition that can be true of false. **Complex** sentences are constructed from simpler sentences, using parentheses and logical connectives ($\neg, \land, \lor, \Rightarrow, \Leftrightarrow$)

![](../img/2019-11-25-15-19-38.png)

## Semantics

The semantics defines the rules for determining the truth of a sentence with respect to a particular model. The semantics for propositional logic must specify how to compute the truth value of any sentence, given a model. This is done recursively, by specify how to compute the truth of atomic sentences and how to compute the truth of sentences formed by 5 connectives.

![](../img/2019-11-25-15-35-45.png)

## A simple knowledge base

![](../img/2019-11-25-15-40-57.png)

Sentences in $KB$:

![](../img/2019-11-25-15-43-29.png)

## A simple inference procedure

Our goal is to decide whether $KB \models \alpha$ for some sentences $\alpha$.

Our first algorithm for inference is a model-checking approach that is a direct implementation of the definition of entailment: enumerate the models, and check that $\alpha$ is true in every model in which $KB$ is true.

![](../img/2019-11-25-15-48-48.png)

![](../img/2019-11-25-15-50-02.png)

# Propositional Theorem Proving

Entailment can be done using **theorem proving** -  applying rules of inference directly to the sentences in our knowledge base to construct a proof of the desired sentence without consulting models.

**Logical equivalence:**

$$ \alpha \equiv \beta \text{ iff } \alpha \models \beta \text{ and } \beta \models \alpha$$

**Validity** (Tautologies): A sentence that is true for all models.

**Satisfiability**: a sentence is true in some model. The problem of determining the satisfiability of sentences in propositional logic (**SAT**) was the first problem proved to be $NP$-complete.

## Inference and Proofs

![](../img/2019-11-25-16-09-02.png)

The notion means that, whenever any sentences of the form $\alpha \Rightarrow \beta$ and $\alpha$ are given, the then sentence $\beta$ can be inferred.

We can apply any search algorithm to find a sequence of steps that constitutes a proof. We define the problem as follow:

* **Initial state:** The initial knowledge base
* **Actions:** the set of actions consist of all the inference rules applied to all sentences that match the top half the inference rule
* **Result:** The result of an action is to add the sentence in the bottom half of the inference rule.
* **Goal** The goal is a state that contains the sentence need to be proved.

**Monotonicity**: The set of entailed sentences can only increase as information is added to the knowledge base. If $KB \models \alpha$ then $KB \land \beta \models \alpha$

## Proof by Resolution

 **Resolution** is a simple inference rules which yields a *complete* inference algorithm when coupled with any search algorithm.

 The idea of resolution rule is that if you know $\alpha$ or $\beta$, and you know $\neg \beta$ and $\gamma$, then you are allowed to conclude that $\alpha$ or $\gamma$.

 ![](../img/2019-12-02-16-22-37.png)

 The unit resolution rule is 

 ![](../img/2019-12-02-16-23-06.png)

 where $l$ is literal and $l\_i$ and $m$ are complementary literals (one negates the other).

 The full resolution rule is 

 ![](../img/2019-12-02-16-24-47.png)

 where $l\_i$ and $m\_j$ are complementary literals.

 > A resolution-based theorem prover can, for any sentences
$\alpha$ and $\beta$ in propositional logic, decide whether $\alpha \models \beta$

### Conjunctive Normal Form

> Every sentence of propositional logic is logically equivalent to a conjunction of clauses.

A sentence expressed as conjunction of clauses is said to be in **conjunctive normal form**.

### A resolution algorithm

The algorithm works by using contradiction, that is , to show $KB \models \alpha$, we show that $KB \wedge \neg \alpha$ is unsatisfiable.

![](../img/2019-12-02-16-42-17.png)

* First, we convert $KB \wedge \neg \alpha$ into CNF.
* Each pair of the resulting clause that contains complementary literals is resolved produce a new clause, which is added to the set if not already present. The process continues until:
  * There are no new clauses that can be added, in which $KB$ does not entail $\alpha$
  * Two clauses resolve to yield empty clause, in which $KB$ entails $\alpha$


### Completeness of resolution

## Horn clauses and Definite clauses

For practical problems, we might not need the full power of resolution. Some knowledge bases satisfy certain restrictions on the form of sentences they contain, therefore, we may use more restricted and efficient  inference algorithm.

**Definite Clause:** Disjunction of literals of which exactly one is positive. For example, the clause $(\neg A \vee \neg B \vee C)$

**Horn Clause:** Disjunction of literals of which at most one is positive.

Characteristic of $KB$ which contains only definite clause:

* Every definite clause (and Horn clause) can be written as an implication. The premise is the conjunction of positive literals (negation of negative literals) and the conclusion is a single positive literal.
* Inference with Horn clause can be done through **forward-chaining** and **backward-chaining**.
* Deciding entailment with Horn clause can be done in time that is linear of the size of $KB$.

## Forward and backward chaining

Forward chaining is a **data-driven** reasoning.

* It begins from know facts (positive literals) in $KB$.

* If all premises of an implication are known, then its conclusion added to the known set.
* The process continues until the query is in known set or no further inferences can be made.

Backward chaining is **goal-directed** reasoning

* It starts from implications in $KB$ whose conclusion is the query.
* If all the premises of any of those implications can be proved (recursively), then the query is true. 

The cost of **goal-directed** is often much less then linear in the size of $KB$, since it only touches relevant facts.

# Effective Propositional Model Checking

## A complete backtracking algorithm

We add some improvements to the initial backtracking algorithm.

**DPLL** algorithm takes input in CNF and embodies these improvements:

* Early termination: A *clause* is true if any *literal* is true. A *sentence* is false if any *clause* is false. Therefore, we can determine the sentence must be true or false even with partially completed model.
* Pure symbol heuristic: A pure symbol is a symbol that always appear with the same sign in all clauses. We can quickly find the value to assign for pure symbol.
* Unit clause heuristic: means that a clause in which all literals but one are already assigned $false$. We can simplifies the clause to only one literal and find the value to assign for it.

## Local search algorithm

We can apply the idea of hill climbing or simulated annealing. The goal is to find an assignment that satisfies every clauses and the evaluation function counts the number of unsatisfied clauses. The **WalkSAT** algorithm chooses randomly between two ways to pick a symbol to flip:

* *min-conflict* that minimizes the number of unsatisfied clauses in the new state
* *random-walk* that pick the symbol randomly.





