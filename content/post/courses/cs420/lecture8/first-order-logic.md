---
title: "First Order Logic"
date: 2019-12-09T20:58:58+07:00
draft: true
tags: ['cs420']
---

<!--more-->

# Representation

First order logic (FOL) tries to adopt the foundation of propositional logic - declarative and compositional semantics that is context-independent and unambiguous and borrows the representational idea from natural language.

It based on **objects** (as Noun), **relations** (verb), and **functions** (there is only 1 "value" for 1 "input")

# Syntax and Semantics

## Models for FOL

Models for FOL have objects inside. The domain of a model is the set of objects (domain element) it contains and is required not to be empty. 

Relations between objects in a model is described by a set of tuples of objects that are related.

## Symbols & Interpretations

The basic elements of FOL are the symbols that stand for objects, relations and functions.

* **Constant** symbols stand for objects
* **Predicate** symbols stand for relations.
* **Function** symbols stand for function.

Each model includes a interpretation that specifies exactly which objects, predicates, and functions are referred to by the symbols.

## Terms

Term is a logical expression that refers to an object. Constant symbol therefore is term. Complex term can be formed by taking a function of one or more terms. We can think complex term just as a kind of name, not a "subroutine call" that "returns a value"

## Atomic sentences

$$Term + Predicate = Atomic \ Sentence$$

> An atomic sentence is **true** in a given model if the relation referred to by the predicate symbol holds among the objects referred to by the arguments.

## Complex sentences
Logical connective can be used to form complex sentences from atomic sentences.

## Quantifiers

### Universal quantification $\forall$

