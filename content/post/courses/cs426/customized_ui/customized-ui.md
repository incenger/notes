---
title: "Customized UI"
date: 2019-07-11T07:56:52+07:00
lastmod: 2019-07-11T07:56:52+07:00
description: ""
tags: []
---

Customizing UI in Android

<!--more-->
* [ ] Canvas and Drawable

# High Level UI & Low Level UI

* High-Level UI in Android are `TextView`, `ImageView`,...

* Low-level UI: We can extend from `View` class and create own our UI component and use it in xml files.

```Java

<com.example.minhtriettran.customizeduidemo.MyView
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
/>
```
# Animation

We can create animation by drawing continuously

# Translation, Rotation and Zooming

Multi-touch required.

From vector $AB$ to $CD$.

* Translation: Midpoint $M$ of $AB$ to $N$ of $CD$
* Rotation: Angle between $AB$ and $CD$
* Zooming: $\frac{||CD||}{||AB||}$

# Play with Canvas of Bitmap and Double Buffer

* Create a Bitmap. There are two kinds of bitmap: Mutable and Immutable
* Try to get corresponding Canvas object of that Bitmap
* Draw on the Canvas object

```java
void onDraw(Canvas canvas) {
    
    Bitmap backBuffer = new Bitmap.createBitmap(height, width, Bitmap.Config.ARGB_8888);

    // Get the canvas of bitmap
    Canvas myCanvas = new Canvas(tmp);

    // Drawing something on canvas

    // Put the drawn back buffer to the screen.
    canvas.drawBitmap(backBuffer, 0, 0, null);

}
```

Drawing only components that are dynamic.




