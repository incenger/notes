---
title: "CS426"
date: 2019-07-04T08:11:56+07:00
lastmod: 2019-07-04T08:11:56+07:00
description: ""
tags: []
---

<!--more-->

- [X] Watch Droidcon sessions
- [X] Callbacks
- [X] Map, Filters, Reduce
- [ ] Architecture Components in Android

# Model-View-Controller

 ![](2019-07-04-08-18-52.png)

The __Controller__ handles the input from users, therefore, some `on...Listener()` will be implemented in __Controller__. 

However, __View__ is the part that shows information to the user, then it should handles user input. Therefore, we come up with a modification.

 ![](2019-07-04-12-51-12.png)

We can see from the diagram above, the view has to know and hanlde too much things $\rightarrow$ We try to break the access of __View__ to __Model__. 

 ![](2019-07-04-08-25-55.png)

 and let the controller handles everything
 

# Model - View - Presenter

![](2019-07-04-08-28-24.png)

We have a kind of contract (in this case, an `interface`) for the __View__ and __Presenter__ to interact.

* The `interface` for __View__ to update the UI
* The `interface` for __Presenter__ to notify the __View__ to update when the data changes.

![](2019-07-04-08-37-55.png)

 ```java
 // Of View
 interface IView {
     void setName(String name);
 }

// Of Presenter
IUserMode userModel;
IView view;

 Presenter(IView view, IUserModel userModel) {
     this.userModel = userModel;
     this.view = view;
 }

 interface IPresenter {
     // call when data changes.
     onLoad();
 }

 @override
 public void onLoad() {
     // get data from model
     // and set on view
     // using IView and IModel in Presenter
     User user = userModel.getUser();
     String displayName = user.getName();
     view.setName(displayName);
 }

```

We could see the __Presenter__ as the _producer_ of data and the __View__  is the consumer. Then, the question arise is why the _producer_ needs to know about its _consumer_. The _producer_ should only streams the data for its _subcriber_ (__Observer__ pattern).


# Model - View - ViewModel

![](2019-07-04-08-55-11.png)

The __ViewModel__ expose the model for the __View__

# Callbacks

Client provides functions for the module to call them instead calling functions provided by modules.

Using callback requries programming languages in which functions are __first-class__ -  treated as any other values:

* Passed as parameters
* Returns as values
* Stored in variables and data structures

In some OOP languages like `Java`, functions are not __first-class__, however, we could implement __callback__ by passing object carrying functions because _objects_  are __first-class__

# ViewModel, LiveData in Android

# Map -  Filter - Reduce

Work on __stream__ of data.

## Map
Apply unary function (function taking one argument) to each element of the stream and return new stream containing the result in order.

## Filter

Filter test element with and __predicate__, if element satisfies the predicate, then it is not removed. A new sequence is returned. __Filter__ does not change the input sequence.


## Reduce
Combine elements of stream together using binary function. In addition the function and the stream, __Reduce__ also takes an argument as initial values.

`stream.reduce(init, f)` works:

> result0 = init 

> result1 = f(result0, stream[0]) 

> result2 = f(result1, stream[1]) 

> ... 

> resultn = f(resultn-1, stream[n-1])

### Initial Value
The inital value is required or not is the implementation choice.


### Order of operations

If the operation is associative, the order is not important (like `+` or `max`).
Usually, we have 
* __Fold-left__ 
> result0 = init 

> result1 = f(result0, stream[0]) 

> result2 = f(result1, stream[1]) 

> ... 

> resultn = f(resultn-1, stream[n-1])

* __Fold-right__

> result0 = init 

> result1 = f(stream[n-1], result0) 

> result2 = f(stream[n-2], result1) 

> ... 

> resultn = f(stream[0], resultn-1)

### Return type

* Accumulator: add an elements (stream of type `E`) to the growing result (stream of type `F`)

* Combiner: add two partial results (both stream of type `E`) to the growing result (type `E`)
