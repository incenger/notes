---
title: "Bayesian Statistical Inference"
date: 2019-10-10T09:53:57+07:00
draft: true
tags: ['mit6041sc', 'statistics', 'probability']
---

- [ ] Compare MAP and MLE
- [ ] 

<!--more-->

# Classical and Bayesian Inference

**I think I'll update this section later**

Two approaches to estimate an unknown quantity

* The classical (frequentist) views the unknown quantity $\theta$ as a constant that happens to be unknown. They then try to develop an estimator for $\theta$ that has some performance guarantees.
* The Bayesian views $\theta$ as a R.V with known (**prior**) distribution. Given the observed data $x$, they will use Bayes' rule to update they initial belief about the distribution of $\theta$ (that is the **posterior** $p\_{\Theta | X}(\theta| x)$).


# Bayesian inference and the Posterior distribution.

> Summary of Bayesian Inference
> 
> * We start with a prior distribution $p\_{\Theta} (f\_{\Theta})$ for the unknown variable $\Theta$
> * We have a model $p\_{X|\Theta} (f\_{X | \Theta})$ of the observation vector $X$
> * After observing value $x$ of $X$, we form the posterior distribution of $\Theta$, using the Bayes' rule

# Point estimation, Hypothesis testing, and the MAP rule

##  MAP (Maximum a Posterior Probability)

Given the value $x$ of the observation, we select a value of $\theta$, denoted $\hat{\theta}$, that maximize the posterior distribution.

## Point Estimation

Given the posterior distribution of $\Theta$ we want to use a single numerical value that represents our best guess of the value of $\Theta$, often denoted $\hat{\theta}$. We apply some function $g$ to the observation $x$, to get the value of $\hat{\theta} = g(x)$. Since $x$ is an observation of R.V $X$,  $\hat{\Theta} = g(X)$ is a R.V called **estimator**. Different functions $g$ give us different estimators

* **MAP** estimator sets the estimate $\hat{\theta}$ to the value of that maximize the posterior distribution over all possible values of $\theta$.
* Conditional Expectation (LMS) sets the estimate $\theta$ to $E[\Theta | X = x]$.

## Hypothesis Testing

In hypothesis testing problem, $\Theta$ takes one of $m$ values $\theta_1, \dots, \theta_m$ where $m$ is usually a small integer. Once the value $x$ of $X$ is observed, we can calculate the posterior probabilities $p\_{\Theta|X}(\theta\_i | x)$ for each $i$. According to **MAP** rule, we may select the hypothesis which gives the highest posterior probability.