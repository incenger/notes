---
title: "Large Numbers"
date: 2019-10-02T14:37:12+07:00
tags: ['mit6041sc', 'probability', 'statistic']
---

<!--more-->
- [x] Visualizing X and $M\_n$.
- [x] Visualizing CLT with uniform and geometric, try with $CDF$


# Markov and Chebyshev Inequalities

## Markov Inequalities

If a non-negative random variable has a small mean, the the probability it has large value must be small

> If a random variable $X$ can only take non-negative values, then

> $$ P(X \geq a) \leq  \frac{E[X]}{a} $$

> for all $a > 0$.

__Proof:__

Let fix number $a$ and consider the random variable $Y\_a$ defined by

 $$ Y_a =
\begin{cases}
0,  & \text{if $X < a$} \\\\\\
a, & \text{if $X \geq a$}
\end{cases}$$

The relation $ Y\_a \leq X$ always holds, therefore

$$E[Y\_a] < E[X]$$

On the other hand, 
$$E[Y_a] = aP(Y_a = a) = aP(X \geq a)$$

from which we obtain
$$aP(X \geq a) \leq E[X] $$

## Chebyshev Inequality

> If $X$ is a random variable with mean $\mu$ and variance ${\sigma}^{2}$, then
> $$P(|X - \mu| \geq c) \leq  \frac{\sigma^2}{c^2} \text{for all $c > 0$}$$  

__Proof:__ Textbook

__Loose inequalities:__ Even though Chebyshev inequality tends to be more strict than Markov inequality, however, both of them only use mean and variance, which are rough summaries property of the distribution, hence we cannot expect the bounds to close approximations to the exact probabilities.


# The weak law of large numbers

The weak law of large numbers asserts that the sample mean of a large number of __independent identical__ distributed random variables is very close to the true mean with high probability.

Let 
$$ M_n =  \frac{X_1 + \dots + X_n}{n} $$

We have 
$$E[M_n] = \mu $$
and,
$$var(M_n) =  \frac{\sigma^2}{n}  $$

Applying Chebyshev inequality, we obtain __The Weak Law of Large Numbers__


> $$P(|M_n - \mu| \geq \epsilon ) \leq  \frac{\sigma^2}{n \epsilon^2}  $$


For fixed $\epsilon > 0$, the right-hand side goes to $0$ as n increase to infinity.

# Convergence in Probability

> Let $Y_1, Y_2$, ... be a sequence of random variables (not necessarily independent), and let $a$ be a real number. We say that the sequence $Y_n$ __converges to__ $a$ __in probability__, if for every $\epsilon > 0$, we have

> $$ \lim\_{n \rightarrow \infty}(P(|Y_n - a| \geq \epsilon)) = 0 $$


For every $\epsilon > 0$

$$\lim\_{n \rightarrow \infty} P(|Y\_n - a| \geq \epsilon ) = 0$$

For every $\epsilon > 0$ and for every $\delta > 0$, there exists some $n_0$ such that

$$ P(|Y_n - a| \geq \epsilon) \leq \delta $$

we can refer to $\epsilon$ as __accuracy__ level and $\delta$ as __confidence__ level.

When $Y_n$ converges to $a$, we are not certain about $E[Y_n]$ also converge to $a$.

__Example:__ $M_n =  \frac{X_1 + \dots +X_n}{n} $ with $X_i$ is binomial R.V with $p = 0.4$ and $n = 100, 200, \dots, 2000$ 

![](../img/Figure_1.png)

# Central Limit Theorem

> Let $X_1, X_2$,... be a sequence of independent identical distributed random variables with common mean $\mu$ and variance ${\sigma}^{2}$ and define
> $$Z_n =  \frac{S\_n - E[S\_n]}{\sigma\_{S\_n}}  =   \frac{X_1 + \dots + X_n - n\mu}{\sigma \sqrt{n}} $$
> Then, the CDF of $Z_n$ converges to the standard normal CDF in the sense that
> $$ \lim\_{n \rightarrow \infty}(Z_n \leq z) = \Phi(z) $$

* $E[Z_n]  = 0$
* $Var(Z_n) = 1$
* We only have statement about $CMF$ not $CDF$


__Visualization:__

* Uniform distribution
    ![](..//img/Figure_3.png)
* Exponential distribution
    ![](..//img/Figure_4.png)














