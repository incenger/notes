---
title: "STAT452 Notes "
date: 2019-10-09T13:12:31+07:00
draft: true
tags: ['stat452']
---

<!--more-->

- [ ] Unbiased estimator
- [ ] Probability Distribution: Hypergeometric, Poisson, Chi Squared, t-student, Fisher

# $\chi^2$ distribution

$\chi^2$ distribution with $k$ degrees of freedom is the distribution of a sum of the squares of $k$ independent standard normal random variables

Related to **variance**

# $t$-student

$X \approx N(0, 1)$

$Y \approx \chi^2(n)$

$T = \frac{X}{\sqrt{\frac{Y}{n}}} \approx t(n)$

Find CI for $\mu$ using $S\_x$ in stead of $\sigma$

# Fisher

$X \approx \chi^2(m)$

$Y \approx \chi^2(n)$

$\frac{\frac{X}{m}}{\frac{Y}{n}} \approx Fisher(m, n)$

# Estimated interval for a distribution

If $P(a < X < b) = p$ then $(a, b)$ is estimated interval for $X$ at probability $p$.

# Sampling Distribution

Theorem: Lindeberg - Levy (Normal Population)

$X_1, \dots, X_n$ are normal distribution R.V

* $\overline{X} \approx N(\mu, \frac{\sigma^2}{n})$

# Interval Estimation


## Estimation parameter $\lambda$ of population

Given a random sample $X_1, \dots, X_n$, find **PIVOT STATISTIC** $T(\lambda) = T(\lambda, X_1, \dots, X_n)$ in which the distribution of $T(\lambda)$ is well-defined.

With given $\gamma$, find $\[ a, b \]$ such that $\gamma = P(a \leq T(\lambda) \leq b)$. We can derive $P(c \leq \lambda \leq d) = \gamma$. $\[c, d\]$ is the confidence interval for $\lambda$ with confidence coefficient $\gamma$.

### Estimation for population mean

If $X_1, X_2, \dots, X_n$ are normally distributed with mean $\mu$ and variance $\sigma^2$, then

$$T = \frac{\bar{X} - \mu}{S/\sqrt{n}}$$

has t-distribution with $n-1$ degrees of freedom

[**Proof Idea**](https://newonlinecourses.science.psu.edu/stat414/node/199/)

### Estimation for variance

If $X_1, X_2, \dots, X_n$ are normally distributed with mean $\mu$ and variance $\sigma^2$, then

$$\frac{(n-1)S^2}{\sigma^2}$$

has $\chi^2$ probability distribution with $n-1$ df.


### Estimation for proportion

$$\frac{\hat{p} - p}{\sqrt{p(1-p)/n}}$$

has standard normal  distribution.

For large sample, we cap approximate $\sqrt{p(1-p)/n} \approx \sqrt{\hat{p}(1-\hat{p})/n}$

# Hypothesis testing

> 1. Formulate the null hypothesis $H_0$ and the alternative hypothesis $H_a$. $H_0$ is tested for possible rejection under assumption that it is true.
> 2. Identify a test statistic that can be used to assess the truth of null hypothesis. The important property of test statistic is that its sampling distribution under the null hypothesis must be calculated, which allows $p$-value to be calculated.
> 3. Compute the rejection region for significance level $\alpha$
> 4. Compare test statistic and rejection region to reject or not reject the null hypothesis.

## Test statistic for mean, proportion and variance

Use the statistics above.

## Test statistics for two samples

Before testing whether the means of two samples is equal, we must test whether their variance is equal.

### Variances of two samples

Let $X_1, \dots,  X_m$ be a random sample from a normal distribution with variance $\sigma\_1^2$, let $Y_1, \dots,  Y_m$ be another random sample (independent of the $X_i$s) from a normal distribution with variance $\sigma\_2^2$, and let $S\_1^2$ and $S\_2^2$  denote the two sample
variances. Then the rv
$$F = \frac{S\_1^2/\sigma\_1^2}{S\_2^2/\sigma\_2^2}$$

has an $F$ distribution with $v_1 = m-1$ and $v_2 = n-1$

Therefore, if the null hypothesis is $H_0: \sigma\_1^2 = \sigma\_2^2$, the test statistic is $f = \frac{S\_1^2}{S\_2^2}$

### Means of two samples

[Proof](https://newonlinecourses.science.psu.edu/stat414/node/201/)

Let $S_p^2$ is the **pooled sample variance**

$$S\_p^2 = \frac{(m-1)S\_1^2 + (n-1)S\_2^2}{m+n-2}$$

then 
$$T  = \frac{(\hat{X} - \hat{Y}) - (\mu\_X - \mu\_Y)}{S_p\sqrt{\frac{1}{m} + \frac{1}{n}}}$$

has $t$-distribution with $m+n-2$ df.

# ANOVA (Analysis of Variance)

$H_0: \mu_1 = \dots = \mu_n$

$H_a:$ at least two of the $\mu_i$ are different.

Sample:

* Population $1$ (Treatment $1$): $X\_{11}$, $X\_{12}$, ..., $X\_{1J}$ $\approx N(\mu_1, \sigma^2)$
* Population $i$ (Treatment $j$): $X\_{i1}$, $X\_{i2}$, ..., $X\_{iJ}$ $\approx N(\mu_i, \sigma^2)$
* ...
* Population $I$ (Treatment $I$): $X\_{I1}$, $X\_{I2}$, ..., $X\_{IJ}$ $\approx N(\mu_I, \sigma^2)$

For each population, we have

$$\overline{X_i} \approx N(\mu_i, \frac{\sigma^2}{J})$$

$$\frac{SSE}{\sigma^2} = \frac{(J-1)S_i^2}{\sigma^2} \approx \chi^2(J-1)$$

Error Sum of Square: Sum of all square errors

$SSE = (J-1)\sum_{i=1}^{I}S\_i^2$

$\frac{SSE}{\sigma^2} \approx \chi^2(J(I-1))$

If $H_0$ is true, $\bar{X_i} \approx N(\mu, \frac{\sigma^2}{J})$

Therefore, 

$$\overline{X\_{..}} = \frac{1}{I} \sum\_{i=1}^{I}X\_i = \frac{1}{IJ} \sum \sum X\_{ij} \approx N(\mu, \frac{\sigma^2}{IJ})$$

$$\frac{(I-1)S^2}{\sigma^2/J} \approx \chi^2(I-1)$$

$$S^2 = \frac{1}{I-1} \sum\_{i=1}^{I}(\bar{X\_i} - \bar{X})^2$$

$$\frac{SSTr}{\sigma^2} \approx \chi^2(I-1)$$

Therefore

$$F = \frac{\frac{SSTr}{\sigma^2}/(I-1)}{\frac{SSE}{\sigma^2}/(I(J-1))} = \frac{MSTr}{MSE} \approx F(I-J, I(J-1))$$

# Linear Regression

The population regression function (**PRF**): $y = \beta\_0 + \beta\_1 x$

Since we only have sample data, not the whole population, we find sample regression function (**SRF**) $\hat{y} = \hat{\beta}\_0 + \hat{\beta}\_1 x$

For each sample, we will can find $\hat{\beta}\_0$ and $\hat{\beta}\_1$ using **OLS**. Hence, we care about the population of $\hat{\beta}\_0$ and $\hat{\beta}\_1$.


## Simple regression

$$y = \beta_0 + \beta_1 x$$

## Multiple regression


$$y = \beta\_0 + \beta\_1 x\_1 + \dots + \beta\_k x\_k$$


# Test on proprotion of data

**Pearson Theorem**: $\chi^2$ test

$H_0$: $p\_1 = p\_{10}, \dots, p\_k = p\_{k0}$

* Observed data: $n_1, n_2, \dots, n_k$
* Expected data: $np\_{10}, \dots, np\_{k0}$

$$A = \Sigma_{i=1}^{k} \frac{(n\_i - np\_{i0})^2}{np\_{i0}} \approx \chi^2(k-1)$$

To test for a pdf of X: $H\_0: X \approx f(x)$

Dividing range of $X$ into many regions:

* $p_{i0} = P(X \in I\_i)$

Then this is equivalent to original problem, which can be solved by Pearson theorem

---

$H\_0:$ $X$ has normal distribution $N(\mu, \sigma^2)$

Estimating $\mu$ and $\sigma^2$ by $\bar{X}$ and ${S\_X}^2$

We decrease 2 $df$ since we have done two estimating.

## Test for homogeneity

Contingency table

## Test for independency

# Review Final

## Textbook

[Principles of Econometrics 4th ed](https://danboak.files.wordpress.com/2017/09/principles-of-econometrics-4th-edition.pdf)

## Exercise 1

The coefficient of determination $r^2$

$$r^2 = \frac{SSR}{SST} = 1 - \frac{SSE}{SST}$$

* $SSR = \sum(\hat{y} - \bar{y})^2$ - Sum of regression
* $SSE = \sum(y - \hat{y})^2$ - Sum of squared errors
* $SST = \sum(y - \bar{y})^2$ - Total sum of squares
  
The estimation of $\sigma^2$

$$\hat{\sigma}^2 = S^2 = \frac{SSE}{n - k}$$

* $k$ is the number of parameters of the model. For example, the simple linear regression $y = \beta_0 + \beta_1x$ has $k=2$.

* $n$ is the number of observation.

## Exercise 2

The least square estimate of the intercept $\beta\_0$ and the slope parameter $\beta_1$ in simple linear regression model:

$$\hat{\beta\_1} = \frac{\sum(x\_i - \bar{x})(y\_i - \bar{y})}{\sum(x\_i - \bar{x})^2} = \frac{S_{xy}}{S\_{xx}}$$

$$\hat{\beta\_0} = \frac{\sum y\_i - \hat{\beta\_1}\sum x\_i}{n}$$


## Exercise 4 

The $t$-statistic is calculated using the formula

$$ t = \frac{\hat{\beta\_i}}{\hat{s}\_{\beta\_i}}$$

* $\hat{s}\_{\beta\_i}$ is the standard estimated error of $\hat{\beta\_i}$


The pivot statistic 
$$t = \frac{\hat{\beta_i} - \beta\_i}{\hat{s}\_{\beta\_i}}$$

has a $t$ distribution with $n-k$ df.

Therefore, the confidence interval for model parameter estimate $\beta\_i$ is:

$$\hat{\beta\_i} \pm t\_{\alpha/2, n - k} \times \hat{s}\_{\beta\_i}$$

For hypothesis testing, just simply use the pivot statistic with appropriate alternative hypothesis.

## Exercise 6

The estimated covariance matrix:

* Diagonal contains the estimated variance of each parameter
* Number at row $i$ and column $j$ is the covariance of $\beta_i$ and $\beta_j$.

Standard error is the estimation of standard deviation, hence can be calculated from the table.

To perform hypothesis testing with linear combination of parameters, e.g $\beta_1 + 3\beta_2$, we use the test statistic:

$$t = \frac{\hat{\beta_1} + 3\hat{\beta_2}}{SE(\hat{\beta_1} + 3 \hat{\beta\_2})}$$

* The standard error can be calculated using the estimated covariance matrix. For example: 

$$SE(\hat{\beta_1} + 3 \hat{\beta\_2}) = \sqrt{Var(\hat{\beta_1} + 3 \hat{\beta\_2})}$$

  * Variance of linear combination can be calculated by

    $$Var(aX+bY) = a^2Var(X) + 2abCov(X,Y) + b^2Var(Y)$$

## Exercise 7

The standard errors of  $\hat{\beta\_2}$ and $\hat{\beta\_3}$ formulas:

$$SE(\hat{\beta\_2}) = \sqrt{\widehat{Var{\beta\_2}}} = \sqrt{\frac{\hat{\sigma}^2}{\sum(x\_{i2} - \bar{x_2})^2(1 - r^2\_{23})}}$$

$$SE(\hat{\beta\_3}) = \sqrt{\widehat{Var{\beta\_3}}} = \sqrt{\frac{\hat{\sigma}^2}{\sum(x\_{i3} - \bar{x_3})^2(1 - r^2\_{23})}}$$

## Exercise 8

The marginal effects tell us how the outcome variable change when an explanatory variable changes. It can be calculated by taking the derivative of the outcome respect to the explanatory variable $\frac{\partial y}{\partial \beta\_i}$.



[Solution (Page 24)](http://www.principlesofeconometrics.com/poe4/answers/poe4answers.pdf)

## Exercise 9

For the part (iv) of question (d), we need to perform inference on an nonlinear combination of coefficient. For example:

$$\lambda = \frac{\beta\_1}{\beta\_2}$$

The pivot statistic is still the same:

$$\frac{\hat{\lambda} - \lambda}{SE(\hat{\lambda})} \approx t\_{N-K}$$

Therefore, the difficult part is calculating the $SE(\hat{\lambda})= \sqrt{Var(\hat{\lambda})}$

The approximate variance expression below holds true for all nonlinear functions of two estimators:

$$Var(\hat{\lambda}) = \left(\frac{\partial \lambda}{\partial \beta_1}\right)^2Var(\hat{\beta\_1}) + \left(\frac{\partial \lambda}{\partial \beta_2}\right)^2Var(\hat{\beta\_2}) + 2\left(\frac{\partial \lambda}{\partial \beta_1}\right)\left(\frac{\partial \lambda}{\partial \beta_2}\right)Cov(\hat{\beta\_1}, \hat{\beta\_2}) $$

## Exercise 10

The $F$ test for restricted model. The $F$ statistic given by 

$$F = \frac{(SSE\_R - SSE\_U)/J}{SSE\_U/(N-K)}$$

* $SSE\_U, SSE\_R$ is the SSE of unrestricted and restricted model respectively.
* $J$ is the number of restriction.

If the **null hypothesis** is true then the $F$ statistic has $F$-distribution with $J$ numerator df and $N-K$ denominator df.


When $J = K-1$, which means that we that we want to test $\beta\_2 = 0, \beta\_3 = 0, \dots, \beta\_K = 0$ (test of overall significance of the regression model), the $SSE\_R$ becomes:

$$SSE\_R = \sum (y\_i - \beta\_1^{*})^2 = \sum(y\_i - \bar{y}) = SST$$


## Exercise 13

For part (g) of the problem, $AIC$ stands for Akaike information criterion. This may not involve in the final examination :/

## Exercise 14

[Solution](http://www.principlesofeconometrics.com/poe4/answers/poe4answers.pdf)

![](../img/2020-01-03-11-11-29.png)
