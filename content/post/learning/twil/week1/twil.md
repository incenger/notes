---
title: "This week I Learn: 2019-07-14"
date: 2019-07-14T21:12:51+07:00
lastmod: 2019-07-14T21:12:51+07:00
draft: false
description: ""
tags: ["TWIL"]
---

Linh tinh về những thứ tôi học được trong tuần này.


<!--more-->

# TWIL

Tôi dự định mỗi tuần sẽ viết về những thứ tôi học được trong tuần. Mục đích chủ yếu là để tự  mình review lại những nội dung đã học. Những post trong mục này vẫn đang trong giai đoạn thử nghiệm, do vậy nó sẽ không được thống nhất lắm.


# Linear Algebra

Tuần này tôi hoàn thành được 3 lecture trong khóa MIT 18.06

* Orthogonal Matrices and Gram-Schimdt
* Properites of Determinants
* Determinants formulas and cofactors

## Orthogonal Matrices


Những vector $q_1$, $q_2$,..., $q_n$ được gọi là orthogonal nếu 

* 1 vector $q_i$ bất kì đều vuông góc với $n-1$ vectors còn lại 
* $||q_i|| =  1 \forall i$

Những vectors đó sẽ tạo thành 1 matrix $Q$. Dễ dàng, chúng ta thấy $Q$ gồm những những independant columns (hay nói cách khác Q chứa basis vectors). Tính chất quan trọng nhất của $Q$ chính là:

$$Q^{T}Q = I (1)$$

* $(1) \Rightarrow  Q_{T} = Q^{-1}$

Nếu $Q$ là một square matrix, khi đó chúng ta thấy rõ ràng $Q$ spans full scpace of $N$ dimension. Vì thế, projection on $Q$ is itself. (Projection Matrix: $P =  I$).

Việc sử dụng $Q$ sẽ giúp chúng ta tính toán projection of a vector onto column spaces of matrix is much easier. Bởi vì

$$Q^{T} Q \hat{x} = Q^{T} b$$

sẽ trở thành

$$ \hat{x} = Q^{T} b$$

lúc này tính toán $x$ sẽ dễ hơn rất nhiều.

## Gram-Schmidt

Câu hỏi đặt ra bây giờ là với một column space cho trước, làm sau chúng ta có thể tìm được một matrix chứa orthogonal columns which spans the same column space.

Ý tưởng của Gram-Schmidt

* Với mỗi column, chúng ta sẽ lấy column đó trừ đi its projection on previous orthogonal columns.

Thuật toán

* Vector đầu tiên chúng ta sẽ giữ nguyên, gọi nó là $A$. Orthogonal tương ứng sẽ là $q_a = \frac{A}{||A||}$
* Tiếp tục với second column, chúng ta lấy nó trừ đi projection của nó trên $A$ (hoặc $a$). Khi đó $B = b - \frac{A^T b}{A^T A}A$. Orthogonal tương ứng sẽ có được khi chúng ta lấy vector đơn vị bằng cách chia cho độ dài của vector.

Khi đó, chúng ta sẽ có:
$$A = QR$$
Chúng ta có thể dễ dàng tính được $R = Q^T A$ và những giá trị trên đường chéo chính bằng độ dài của orthogonal vectors thứ $i$.

Trong đó, $ R$ sẽ có dạng

$$ 
\begin{bmatrix}
 q_1^T a & q_1^T b   & q_1^T c \\\ 0 &  q_2^T b & q_2^T c \\\  0 &  0 & q_3^T c \end{bmatrix}
$$

```pseudocode
for j = 1:n                             
    v = A(:, j);                        % column i of A
    for i = 1:j-1                       % previous (i-1) orthogonal q
        R(i, j) = Q(:,i)'*v;            % Computing Rij
        v = v - R(i, j)*Q(:, i);        % subtract projection
    end
    R(j, j) = norm(v);                  % diagonal of R is the length
    Q(:, j) = v/R(j, j);                % divide by length to get orthogonal
end
```

## Properties of Determinant

Determinant là một đại lượng đặc biệt dành cho square matrix. Có 3 tính chất cơ bản và quan trọng nhất của determinant:

* $(1)$ Determinat of Identity: $det(I) =  1$
* $(2)$ Exchange rows reverse the sign of determinant
* $(3)$ Linear on each row
    * $(3a)$  $$ \begin{bmatrix}
ta & tb  \\\ c & d 
\end{bmatrix} 
= t \begin{bmatrix} a & b  \\\ c & d \end{bmatrix} $$

    * $(3b)$  $$ \begin{bmatrix}
a + a' & b + b'  \\\ c & d 
\end{bmatrix} 
= \begin{bmatrix} a & b  \\\ c & d \end{bmatrix} + \begin{bmatrix} a' & b'  \\\ c & d \end{bmatrix} $$

Từ những tính chất đó, chúng ta có thể suy ra một số tính chất sau:

* $(4)$  Nếu matrix chứa 2 row bằng nhau $\Rightarrow det(A) = 0$ 
    * Sử dụng tính chất 2, chúng ta sẽ exchange 2 equal rows, khi đó chúng ta sẽ có $det(A) = -det(A) \Rightarrow det(A) = 0$ 

* $(5)$  $row(i) = row(i) - t \times row(j)$ không làm thay đổi giá trị của determinant
    * $$ \begin{bmatrix}
a  & b   \\\ c - ta & d -ta 
\end{bmatrix} 
= \begin{bmatrix} a & b  \\\ c & d \end{bmatrix} - t \begin{bmatrix} a & b  \\\ a & b \end{bmatrix} = \begin{bmatrix} a & b  \\\ c & d \end{bmatrix} $$

* $(6)$  Nếu có 1 row bao gồm toàn $0$ thì $det(A) = 0$
    *  $$ \begin{bmatrix}
0  & 0   \\\ c & d 
\end{bmatrix} 
= \begin{bmatrix} t \times 0 & t \times 0  \\\ c & d \end{bmatrix} \Rightarrow det(A) = t \times det(A) \Rightarrow det(A) = 0 $$

* $(7)$  Determinant of digonal matrix $=$ products of diagonal
    * Tính chất này có thể dễ dàng suy ra trực tiếp từ tính chất $(1)$ và $(3a)$
    * Như vậy, chúng ta có thể dùng elimination để tìm determinant

* $(8)$  $det(A) = 0$ nếu $A$ là singular matrix, ngược lại $det(A) \neq 0$ nếu $A$ is invertible
    * Nếu $A$ is singular, chúng ta có thể dùng elimination để có được 1 row có tất cả số bằng 0.
    * Ngược lại, nếu $A$ is invertible, chúng ta có thể tìm được tất cả pivot columns và chúng khác $0$.

* $(9)$ $det(AB) = det(A)det(B)$
* $(10)$ $det(A^T)  = det(A)$

## Determinant Formula

__Ý tưởng:__

 $$ \begin{bmatrix}
a  & b   \\\ c & d
\end{bmatrix} 
= \begin{bmatrix} a & 0  \\\ c & d \end{bmatrix} +  \begin{bmatrix} 0 & b  \\\ c & d \end{bmatrix} $$ 
$$
= \begin{bmatrix} a & 0  \\\ c & 0 \end{bmatrix} + \begin{bmatrix} a & 0  \\\ 0 & d \end{bmatrix} + \begin{bmatrix} 0 & b  \\\ c & 0 \end{bmatrix} + \begin{bmatrix} 0 & b  \\\ 0 & d \end{bmatrix} $$
$$
= ad -bc
$$

* Chúng ta có thể lần lượt tách những giá trị của từng hàng ra để được những diagonal matrix. Đa số những matrix chúng ta nhận được sẽ bằng 0. Có chính xác $n!$ matrices có giá trị khác $0$.

$$ det(A) = \sum_{n! \space terms} \pm  a\_{1 \alpha} a\_{2 \beta} a\_{3 \gamma}... a\_{n \omega}  $$

with $(\alpha, \beta, \gamma, ..., \omega) = Perm(1, 2, 3, ..., n)$

Từ công thức trên, nếu chúng ta nhóm để lấy nhân tử chung là một phần tử $a_{ij}$, chúng ta có được công thức cofactors:

$$Cofactor(a_{ij}) = (-1)^{i+j} \times det(matrix \space erased \space row \space i \space and \space column \space j)$$




